package com.tripcups.xmas3.clicker.custom_views;

import android.content.Context;
import android.util.AttributeSet;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;

public class FadingRecyclerView extends RecyclerView {
    public FadingRecyclerView(Context context) {
        super(context);
    }

    public FadingRecyclerView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public FadingRecyclerView(Context context, @Nullable AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

//    @Override
//    protected float getTopFadingEdgeStrength() {
//        return 0.0f;
//    }


    @Override
    public int getPaddingBottom() {
        return 0;
    }
}
