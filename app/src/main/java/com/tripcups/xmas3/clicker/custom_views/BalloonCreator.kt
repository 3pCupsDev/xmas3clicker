package com.tripcups.xmas3.clicker.custom_views

import android.view.View
import androidx.annotation.LayoutRes
import androidx.core.content.ContextCompat
import androidx.fragment.app.FragmentActivity
import com.skydoves.balloon.ArrowConstraints
import com.skydoves.balloon.ArrowOrientation
import com.skydoves.balloon.Balloon
import com.skydoves.balloon.BalloonAnimation
import com.tripcups.xmas3.clicker.R


object BalloonCreator {


    fun create(
        ctx: FragmentActivity,
        @LayoutRes layout: Int,
        balloonWidth: Float,
        balloonHeight: Int,
        marginBottom: Int = 0,
        balloonAlpha: Float = 1f,
        bindView: (view: View, balloon: BalloonCreator) -> Unit
    ) {
        Balloon.Builder(ctx)
            .setArrowSize(22)
            .setLayout(layout)
            .setArrowOrientation(ArrowOrientation.BOTTOM)
            .setArrowConstraints(ArrowConstraints.ALIGN_ANCHOR)
            .setArrowPosition(0.5f)
            .setArrowVisible(true)
            .setAutoDismissDuration(7_000)
            .setDismissWhenLifecycleOnPause(true)
            .setDismissWhenShowAgain(true)
            .setDismissWhenOverlayClicked(true)
            .setDismissWhenClicked(true)
            .setDismissWhenTouchOutside(true)
            .setMarginBottom(marginBottom)
            .setCircularDuration(280)
            .setWidthRatio(balloonWidth)
            .setHeight(balloonHeight + marginBottom)
            .setCornerRadius(14f)
            .setAlpha(balloonAlpha)
            .setBackgroundColor(ContextCompat.getColor(ctx, R.color.white))
            .setBalloonAnimation(BalloonAnimation.CIRCULAR)
            .setLifecycleOwner(ctx)
            .build().apply {
                getContentView().setOnClickListener {
                    dismiss()
                }
                //todo add on dismiss listener
                bindView.invoke(getContentView(), this@BalloonCreator)
            }

    }
}