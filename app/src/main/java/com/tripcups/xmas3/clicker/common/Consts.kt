package com.tripcups.xmas3.clicker.common

import com.tripcups.xmas3.clicker.R

const val MUSIC_VOLUME = 0.24f
const val SFX_VOLUME = 0.42f
const val SPECIAL_SFX_VOLUME = 0.69f
const val MAX_VOLUME = 0.85f

const val BUY_SCREEN = 1
const val TREE_SCREEN = 2
const val STORE_SCREEN = 3

const val BUY_SCREEN_POS = BUY_SCREEN - 1
const val TREE_SCREEN_POS = TREE_SCREEN - 1
const val STORE_SCREEN_POS = STORE_SCREEN - 1


const val UPGRADES_SECTION = 1
const val TREES_SECTION = 2
const val SNOWMAN_SECTION = 3
const val SCENES_SECTION = -1


val TAB_TITLES = arrayOf(
    R.string.tab_text_1,
    R.string.tab_text_2,
    R.string.tab_text_3
)

const val SMALL_WAIT_TIME = 420L
const val ONE_SECOND: Long = 1_000L
const val MEDIUM_WAIT_TIME = 1_420L
const val THREE_SECONDS_WAIT_TIME = 3_000L
const val LONG_WAIT_TIME = 4_200L

const val SHARED_PREFS_FILE = "global_file_v1"
const val SHARED_PREFS_COINS_PARAM = "coins"
const val SHARED_PREFS_TREE_LEVEL_PARAM = "treeLvl"
const val SHARED_PREFS_CLICKS_PER_SEC_PARAM = "cps"
const val SHARED_PREFS_COINS_PER_CLICK_PARAM = "cpc"
const val SHARED_PREFS_CURRENT_LEVEL_PARAM = "customTreeLvl"

const val SHARED_PREFS_GAME_LEVEL_PARAM = "gameLvl"
const val SHARED_PREFS_SNOW_MAN_LEVEL_PARAM = "snow_man_lvl"
const val SHARED_PREFS_IS_SNOW_MAN_RUINED_PARAM = "is_snowman_ruined"
const val SHARED_PREFS_IS_BOUGHT_MISSTLETOE_PARAM = "is_bought_misstletoe"
const val SHARED_PREFS_IS_SNOW_MAN_COLLECTABLE_PARAM = "is_snowman_collectable"
const val IS_FIRST_TIME_IN_GAME_PARAM = "isFirstTime"


const val LUCKY_SPINNER_DIALOG_FRAG = "LUCKY_SPINNER_DIALOG_FRAG"


const val POLL_FISH_API_KEY = "e1b9d060-8d08-413c-85bc-3ecc4873a8e8"


const val SHARED_PREFS_IS_BOUGHT_STORE_ITEM = "isItemMine_"
const val SHARED_PREFS_TOTAL_CLICKS_COUNTER = "totalClicksCounter"
