package com.tripcups.xmas3.clicker.common

import android.app.Activity
import android.content.ActivityNotFoundException
import android.content.Intent
import android.net.Uri
import android.util.Log
import com.google.android.play.core.review.ReviewManager
import com.google.android.play.core.review.ReviewManagerFactory


class InAppReviewManager(private val activity: Activity) {

    private var reviewManager: ReviewManager? = null
    private var instance: InAppReviewManager? = null

    init {
        reviewManager = ReviewManagerFactory.create(activity)
    }

    fun getInstance(): InAppReviewManager {
        if (instance == null) {
            instance = InAppReviewManager(activity)
        }
        return instance!!
    }

    fun openReviewDialog() {
        reviewManager?.let{
            val request = it.requestReviewFlow()
            request.addOnCompleteListener { request ->
                if (request.isSuccessful) {
                    // We got the ReviewInfo object
                    val reviewInfo = request.result
                    val flow = it.launchReviewFlow(activity, reviewInfo)
                    flow.addOnCompleteListener { _ ->
                        // The flow has finished. The API does not indicate whether the user
                        // reviewed or not, or even whether the review dialog was shown. Thus, no
                        // matter the result, we continue our app flow.

                        Log.d("wow", "ReviewDialog was successfully initialized == ")

                    }

                } else {
                    // There was some problem, continue regardless of the result.
                    openThisAppStorePage()
                }
            }
        }
    }

    fun openThisAppStorePage() {
        val appPackageName: String = activity.packageName
        openAppStorePage(appPackageName)
    }

    fun openAppStorePage(appPackageName: String) {
        try {
            activity.startActivity(
                Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=$appPackageName"))
            )
        } catch (anfe: ActivityNotFoundException) {
            activity.startActivity(
                Intent(
                    Intent.ACTION_VIEW,
                    Uri.parse("https://play.google.com/store/apps/details?id=$appPackageName")
                )
            )
        }
    }
}
