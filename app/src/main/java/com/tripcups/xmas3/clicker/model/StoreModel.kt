package com.tripcups.xmas3.clicker.model

import com.tripcups.xmas3.clicker.R
import com.tripcups.xmas3.clicker.common.SCENES_SECTION
import com.tripcups.xmas3.clicker.common.SNOWMAN_SECTION
import com.tripcups.xmas3.clicker.common.TREES_SECTION
import com.tripcups.xmas3.clicker.common.UPGRADES_SECTION
import com.tripcups.xmas3.clicker.ui.main.MainActivity

class StoreModel {
    companion object {
        var store = Store()

        const val UPGRADES_SECTION_NAME = "Upgrades"
        const val TREES_SECTION_NAME = "Trees"
        const val SNOWMANS_SECTION_NAME = "Snowmans"
        const val SCENES_SECTION_NAME = "Scenes"

        fun getStore(mainActivity: MainActivity): Store {
            val categories = arrayListOf<Category>(
                buildUpgradesSection(mainActivity),
                buildTreeSection(mainActivity),
                buildSnowmanSection(mainActivity),
                Category(
                    SCENES_SECTION,
                    "Backgrounds",
                    "Switch the backgrounds in order to enjoy the Christmas spirit in different locations ",
                    arrayListOf(
                        StoreItem(-1,"Item", "this is the first item", 420, 0),
                        StoreItem(-1,"Item", "this is the first item", 420, 0),
                        StoreItem(-1,"Item", "this is the first item", 420, 0),
                        StoreItem(-1,"Item", "this is the first item", 420, 0),
                        StoreItem(-1,"Item", "this is the first item", 420, 0),
                        StoreItem(-1,"Item", "this is the first item", 420, 0)
                    )
                ),
                Category(
                    SCENES_SECTION,
                    "Music",
                    "Royalty free music available ",
                    arrayListOf(
                        StoreItem(-1,"Item", "this is the first item", 420, 0),
                        StoreItem(-1,"Item", "this is the first item", 420, 0),
                        StoreItem(-1,"Item", "this is the first item", 420, 0),
                        StoreItem(-1,"Item", "this is the first item", 420, 0),
                        StoreItem(-1,"Item", "this is the first item", 420, 0)
                    )
                )
            )

            store.categories = categories

            return store
        }

        private fun buildSnowmanSection(activity: MainActivity): Category {
            val wink = StoreItem(1,"Wink","Winking snowman with red scarf",5000,R.drawable.wink_snowman)
            val deer = StoreItem(2,"Deer","Snowman with deer horns",25000,R.drawable.reindeer_snowman)
            val gift = StoreItem(3,"Top Hat","Silly snowman with pink gift",100000,R.drawable.snowman_gift)
            val spinning = StoreItem(4,"Spinning","Snowman with spinning head",250000,R.drawable.spinning_snowman)
            val jumping = StoreItem(5,"Jumping","Jumping snowman with green scarf",900000,R.drawable.jumping_snowman_anim)

            wink.currentUpgradeLvl = getStoreItemCurrentLvl(activity, wink)
            deer.currentUpgradeLvl = getStoreItemCurrentLvl(activity, deer)
            gift.currentUpgradeLvl = getStoreItemCurrentLvl(activity, gift)
            spinning.currentUpgradeLvl = getStoreItemCurrentLvl(activity, spinning)
            jumping.currentUpgradeLvl = getStoreItemCurrentLvl(activity, jumping)

            wink.isMine = getIsStoreItemBought(activity, wink)
            deer.isMine = getIsStoreItemBought(activity, deer)
            gift.isMine = getIsStoreItemBought(activity, gift)
            spinning.isMine = getIsStoreItemBought(activity, spinning)
            jumping.isMine = getIsStoreItemBought(activity, jumping)


            return Category(
                SNOWMAN_SECTION,
                SNOWMANS_SECTION_NAME,
                "Beneficial for generating revenue in coins, but it can melt into snow occasionally ",
                arrayListOf(wink, deer, spinning, jumping),
                available = true
            )
        }

        private fun buildUpgradesSection(activity: MainActivity): Category {
            val autoClicker = StoreItem(
                420,
                "Clicker",
                "Auto clicker elf will help you gain coins faster",
                15,
                R.raw.elves,
                upgradeScalar = 124,
                minUpgradeLvl = 1,
                maxUpgradeLvl = 6
            )
            val cookieMan = StoreItem(
                421,
                "Cookie",
                "Will multiply coins per click",
                650,
                R.raw.gif_cookie_man,
                upgradeScalar = 69,
                minUpgradeLvl = 1,
                maxUpgradeLvl = 7
            )
            val misstletoe = StoreItem(
                555,
                "Decor",
                "Decorative mistletoe will increase santa visit rate",
                4200,
                R.drawable.mistletoe_img
            )

            autoClicker.currentUpgradeLvl = getStoreItemCurrentLvl(activity, autoClicker)
            cookieMan.currentUpgradeLvl = getStoreItemCurrentLvl(activity, cookieMan)
            misstletoe.currentUpgradeLvl = getStoreItemCurrentLvl(activity, misstletoe)


            autoClicker.isMine = getIsStoreItemBought(activity, autoClicker)
            cookieMan.isMine = getIsStoreItemBought(activity, cookieMan)
            misstletoe.isMine = getIsStoreItemBought(activity, misstletoe)

            return Category(
                UPGRADES_SECTION,
                UPGRADES_SECTION_NAME,
                "Buy and improve your upgradable items so you could achieve your best Christmas experience! ",
                arrayListOf(
                    autoClicker,cookieMan,misstletoe
//                        StoreItem(421,"Simple", "6.50 coins every second", 75, R.drawable.elf2, getIsStoreItemIsAvailable(mainActivity, storeItemId = 421)),
//                        StoreItem(422,"Regular", "37.5 coins every second", 99, R.drawable.elf4, getIsStoreItemIsAvailable(mainActivity, storeItemId = 422)),
//                        StoreItem(423,"Pro", "192 coins every second", 250, R.drawable.elf8, getIsStoreItemIsAvailable(mainActivity, storeItemId = 423)) //,
//                        StoreItem(424, "Advanced", "More Clicks! Advanced elves clickers", 420, R.drawable.elf16, getIsStoreItemIsAvailable(mainActivity, storeItemId = 424)),
//                        StoreItem(425, "Pro+", "More Clicks! Pro+ elves clickers", 750, R.drawable.elf32, getIsStoreItemIsAvailable(mainActivity, storeItemId = 425))
//                StoreItem("King", "37.5 coins every 1 second", 334420, 0))
                ),
                available = true
            )
        }

        private fun buildTreeSection(activity: MainActivity): Category {//TODO MAKE FIRST TREE BUYABLE AT FIRST of first game
            val firstTree = StoreItem(0,"First", "This is the first tree", 0, R.drawable.first_tree_anim, true, upgradeScalar = 10, minUpgradeLvl = 1, maxUpgradeLvl = 2)
            val simpleTree = StoreItem(666,"Simple", "Simple Xmas tree", 234, R.drawable.simple_tree_anim, upgradeScalar = 4,minUpgradeLvl = 20, maxUpgradeLvl = 23)
            val childishTree = StoreItem(5,"Child", "Childish Xmas tree", 6789, R.drawable.childish_tree_anim, upgradeScalar = 42,minUpgradeLvl = 13)
            val basicTree = StoreItem(1,"Basic", "Big green Xmas tree", 75_000, R.drawable.basic_tree_anim, upgradeScalar = 1500,minUpgradeLvl =  3, maxUpgradeLvl = 7)
            val linesTree = StoreItem(6,"Lines", "Slim Xmas tree with lines", 120_000, R.drawable.lines_tree_anim, upgradeScalar = 42,minUpgradeLvl = 14, maxUpgradeLvl = 17)
            val darkTree = StoreItem(2,"Dark", "Dark green Xmas tree", 666_420, R.drawable.dark_tree_anim, upgradeScalar = 16,minUpgradeLvl = 8, maxUpgradeLvl= 10)
            val cartoonTree = StoreItem(4,"Cartoon", "Cartoon like Xmas tree", 1_200_000, R.drawable.cartoon_anim, upgradeScalar = 42,minUpgradeLvl = 12)
            val shieldTree = StoreItem(3,"Orange", "Orange Xmas tree",99_999_999 , R.drawable.orange_tree, upgradeScalar = 30,minUpgradeLvl = 11)
//            val realTree = StoreItem(7,"Real", "Real looking Xmas tree with decorations",987654321 , R.drawable.real_tree_anim, upgradeScalar = 30,minUpgradeLvl = 77)

            //todo add more trees

            firstTree.currentUpgradeLvl = getStoreItemCurrentLvl(activity, firstTree)
            basicTree.currentUpgradeLvl = getStoreItemCurrentLvl(activity, basicTree)
            darkTree.currentUpgradeLvl = getStoreItemCurrentLvl(activity, darkTree)
            shieldTree.currentUpgradeLvl = getStoreItemCurrentLvl(activity, shieldTree)
            cartoonTree.currentUpgradeLvl = getStoreItemCurrentLvl(activity, cartoonTree)
            simpleTree.currentUpgradeLvl = getStoreItemCurrentLvl(activity, simpleTree)
            childishTree.currentUpgradeLvl = getStoreItemCurrentLvl(activity, childishTree)
            linesTree.currentUpgradeLvl = getStoreItemCurrentLvl(activity, linesTree)
//            realTree.currentUpgradeLvl = getStoreItemCurrentLvl(activity, realTree)



            firstTree.isMine = true
            basicTree.isMine = getIsStoreItemBought(activity, basicTree)
            darkTree.isMine = getIsStoreItemBought(activity, darkTree)
            shieldTree.isMine = getIsStoreItemBought(activity, shieldTree)
            cartoonTree.isMine = getIsStoreItemBought(activity, cartoonTree)
            simpleTree.isMine = getIsStoreItemBought(activity, simpleTree)
            childishTree.isMine = getIsStoreItemBought(activity, childishTree)
            linesTree.isMine = getIsStoreItemBought(activity, linesTree)
//            realTree.isMine = getIsStoreItemBought(activity, realTree)

            return Category(
                TREES_SECTION,
                TREES_SECTION_NAME,
                "Decorate your favorite Christmas tree! ",
                arrayListOf(
                    firstTree,
                    simpleTree,
                    childishTree,
                    basicTree,
                    linesTree,
                    darkTree,
                    cartoonTree,
                    shieldTree,
//                    realTree
                ),
                available = true
            )
        }

        //todo refactor method

        private fun getIsStoreItemBought(mainActivity: MainActivity, storeItem: StoreItem) : Boolean {
            return mainActivity.getIsStoreItemBought(storeItem.name + storeItem.id)
        }

        private fun getStoreItemCurrentLvl(mainActivity: MainActivity, storeItem: StoreItem) : Int {
            return mainActivity.getStoreItemCurrentLvl(storeItem)
        }

        fun getItemFromCategoryByIndex(catIndex: Int, itemIndex: Int): StoreItem? {
            if(catIndex >= store.categories.size || itemIndex >= store.categories[catIndex].storeItems.size){
                return null
            }

            return store.categories[catIndex].storeItems[itemIndex]
        }
    }
}