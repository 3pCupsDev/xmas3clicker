package com.tripcups.xmas3.clicker.common

import android.util.Log
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import com.pollfish.constants.Position
import com.pollfish.main.PollFish
import com.pollfish.main.PollFish.ParamsBuilder
import com.tripcups.xmas3.clicker.BuildConfig
import com.tripcups.xmas3.clicker.ui.main.MainActivity

class PollFishManager {

    interface PollFishManagerListener {
        fun onPollStarted()
        fun onPollFinished()
    }

    companion object {
        fun init(activity: MainActivity, view: View? = null) {
            val rootView = if(view != null && view.visibility == View.VISIBLE) {
                view as ViewGroup
            } else {
                activity.window.decorView as ViewGroup
            }
            val paramsBuilder = ParamsBuilder(POLL_FISH_API_KEY)
                .indicatorPosition(Position.TOP_LEFT)
                .userLayout(rootView)
                .releaseMode(!BuildConfig.DEBUG)
                .pollfishOpenedListener {
                    activity.onPollStarted()
                }
                .pollfishClosedListener {
                    activity.onPollFinished()
                }
                .pollfishCompletedSurveyListener {
                    //todo check if its ok to reward user here
                    val rewardStr = "You Received ${it.rewardValue} " + it.rewardName
                    Log.d("wow", "onSurveyComplete == $rewardStr")
                }
                .pollfishUserRejectedSurveyListener {
                    //todo make sure it won't bug the user after rejecting once
                    Log.d("wow", "onSurveyRejected")
                    PollFish.hide()
                }
                .build()
            PollFish.initWith(activity, paramsBuilder)
        }
    }
}