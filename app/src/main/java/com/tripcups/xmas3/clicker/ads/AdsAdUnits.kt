package com.tripcups.xmas3.clicker.ads

import com.tripcups.xmas3.clicker.BuildConfig

class AdsAdUnits {

    interface AdUnit {
        val adUnitId: String
    }

    companion object {
        const val APPLICATION_ID = "ca-app-pub-7481638286003806~8193458559"
    }

    object Banner : AdUnit {
        private const val PRODUCTION = "ca-app-pub-7481638286003806/5957628719"
        private const val DEBUG = "ca-app-pub-3940256099942544/6300978111"

        override val adUnitId: String
            get() = if (BuildConfig.DEBUG) {
                DEBUG
            } else {
                PRODUCTION
            }
    }

    object OpenApp : AdUnit { //todo implement ad on app open
        private const val PRODUCTION = "ca-app-pub-7481638286003806/5228028641"
        private const val DEBUG = "ca-app-pub-3940256099942544/3419835294"

        override val adUnitId: String
            get() = if (BuildConfig.DEBUG) {
                DEBUG
            } else {
                PRODUCTION
            }
    }
}