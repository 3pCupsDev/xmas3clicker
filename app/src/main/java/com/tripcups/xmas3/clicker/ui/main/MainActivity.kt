package com.tripcups.xmas3.clicker.ui.main

import android.content.Intent
import android.graphics.Color
import android.graphics.Typeface
import android.media.MediaPlayer
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.View
import androidx.core.content.ContextCompat
import androidx.core.content.res.ResourcesCompat
import androidx.viewpager.widget.ViewPager.OnPageChangeListener
import com.emmanuelkehinde.shutdown.Shutdown
import com.github.jinatonic.confetti.CommonConfetti
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.android.material.snackbar.Snackbar
import com.tripcups.xmas3.clicker.BuildConfig
import com.tripcups.xmas3.clicker.R
import com.tripcups.xmas3.clicker.ads.AdsAdUnits
import com.tripcups.xmas3.clicker.ads.AdsActivity
import com.tripcups.xmas3.clicker.cheats.Xmas3Cheater
import com.tripcups.xmas3.clicker.common.*
import com.tripcups.xmas3.clicker.custom_views.CustomViewPager
import com.tripcups.xmas3.clicker.model.StoreItem
import com.tripcups.xmas3.clicker.model.StoreModel.Companion.SNOWMANS_SECTION_NAME
import com.tripcups.xmas3.clicker.model.StoreModel.Companion.TREES_SECTION_NAME
import com.tripcups.xmas3.clicker.model.StoreModel.Companion.UPGRADES_SECTION_NAME
import com.tripcups.xmas3.clicker.ui.buy.BuyFragment
import com.tripcups.xmas3.clicker.ui.lucky_spinner.LuckySpinnerDialogFragment
import com.tripcups.xmas3.clicker.ui.store.StoreFragment
import com.tripcups.xmas3.clicker.ui.tree.TreeFragment
import com.zhpan.indicator.enums.IndicatorSlideMode
import com.zhpan.indicator.enums.IndicatorStyle
import hari.floatingtoast.FloatingToast
import hari.floatingtoast.FloatingToast.GRAVITY_CENTER
import kotlinx.android.synthetic.main.activity_main.*
import kotlin.math.min
import kotlin.random.Random


interface MainActivityListener {


    //fragments
    fun getBuyFragment() : BuyFragment?
    fun getTreeFragment() : TreeFragment?
    fun getStoreFragment() : StoreFragment?
    //smooth scroll listener
    fun goToViewPagerFrag(fragPos: Int = TREE_SCREEN_POS)

    //Main Act Capabilities
    fun popUpText(text: String, newGameLvl: Int)
    fun popUpMessage(text: String)
    fun setToolbarVisibility(isVisible: Boolean)
    fun showMessage(text: String)

    //Coins management
    fun saveCoins(coinsCounter: Int)
    fun getCoins() : Int
    fun addCoins(newPlusCoinsAmount: Int)
    fun subtractCoins(newMinusCoinsAmount: Int)

    //Tree level management
    fun getCurrentTreeLevel() : Int
    fun updateGameLevel()
    fun setTreeLevel(treeLvl: Int)

    fun getCurrentSnowmanLevel(): Int
    fun setCurrentSnowmanLevel(snowManLevel: Int)

    fun getIsRuinedSnowman(): Boolean
    fun setIsRuinedSnowman(isRuined: Boolean)

    fun getMisstleToe(): Boolean
    fun buyMisstleToe()

    fun setCPS(cps: Float)
    fun getCPS(): Float
    fun upgradeCPS() : Float

    fun getCPC() : Int
    fun setCPC(cpc: Int)
    fun upgradeCPC() : Int

    fun setCurrentTree(storeItem: StoreItem)
    fun getStoreItemCurrentLvl(storeItem: StoreItem): Int
    fun setStoreItemCurrentLvl(storeItem: StoreItem, currentTreeLvl: Int)


    fun getGameLevel(): Int
    fun activateStoreTutorial(storeTutorial: Int)

    fun setCurrentSnowMan(storeItem: StoreItem)

    fun startSnow()

    fun getIsFirstTimeInGame(): Boolean
    fun setIsFirstTimeInGame()
    fun setBadgeText(index: Int, badgeText: String?)
}

class MainActivity : AdsActivity(R.layout.activity_main) , StoreFragment.StoreFragmentListener,
    TreeFragment.TreeFragmentListener, BuyFragment.BuyFragmentListener,
    PollFishManager.PollFishManagerListener {

    private var moreBadgeCounter: Int = 0
    private var treeBadgeCounter: Int = 0
    private var storeBadgeCounter: Int = 0

    private var whiteColor: Int = 0
    private var blackColor: Int = 0
    private var customFont: Typeface? = null
    private var currentCoins: Int = 0
    private var currentCps: Float = 0f
    private var currentCpc: Int = 1
    private var currentGameLvl: Int = 1
    private var currentTreeLvl: Int = 1
    private var currentSnowmanLvl: Int = 0
    private var isSnowmanCollectable: Boolean? = null
    private var isFirstTimeInGame: Boolean = true
    private var isSnowmanRuined: Boolean? = null
    private var isMisstleToe: Boolean? = null
    private var totalClicksCounter: Int = 0

    //todo refactor and use 3p cups essentials lib instead
    private var musicPlayer: MediaPlayer? = null
    private var sfxPlayer: MediaPlayer? = null
    private var storeSfxPlayer: MediaPlayer? = null
    private var viewPagerAdapter: ViewPagerAdapter? = null
    private lateinit var viewPager: CustomViewPager
    private var sharedPrefs : SharedPreferencesManager? = null
    public var mHandler: Handler? = null



    private val hideEarnedCoinsText = Runnable {
        earnedCoinsText?.visibility = View.GONE
    }

    private val hideBoughtItemText = Runnable {
        boughtItemText?.visibility = View.GONE
    }

    override val adUnitId: String = AdsAdUnits.Banner.adUnitId


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        mHandler = Handler(mainLooper)

        initSharedPrefs()
        initTextEffects()
        initSFX()
        playMusic()
        initViewPager()
        initVpIndicator()
//        loadStoreStatus()
//        initFab()
        initUi()
        initAds()
        loadBanner(bannerAdContainer)
    }

    private fun initPollFish() {
        mHandler?.postDelayed({
            val isFirstTime = getIsFirstTimeInGame()// && totalClicksCounter <= 0
            if(!isFirstTime) {
                val fragWindow = getBuyFragment()?.view
                PollFishManager.init(this, fragWindow)
            }
        }, SMALL_WAIT_TIME)

    }

    private fun initTextEffects() {
        whiteColor = Color.parseColor("#ffffff")
        blackColor = Color.parseColor("#000000")
    }

    private fun initSFX() {
        sfxPlayer = MediaPlayer.create(this, R.raw.merry_xmas)
        sfxPlayer?.setOnPreparedListener {
            it.setScreenOnWhilePlaying(true)
            it.isLooping = false
            it.setVolume(SFX_VOLUME, SFX_VOLUME)
        }
        storeSfxPlayer = MediaPlayer.create(this, R.raw.success)
        storeSfxPlayer?.setOnPreparedListener {
            it.setScreenOnWhilePlaying(true)
            it.isLooping = false
            it.setVolume(SFX_VOLUME, SFX_VOLUME)
        }
    }

    private fun getTotalClicksCounter(): Int {
        val treeFragment = getTreeFragment()
//        return if(totalClicksCounter == 0){
//            sharedPrefs?.getInt(SHARED_PREFS_TOTAL_CLICKS_COUNTER, 0) ?: 0
//        } else
        return if(treeFragment != null && treeFragment.isAdded) {
            treeFragment.totalClicksCounter
        } else {
            totalClicksCounter
        }
    }

    private fun saveTotalClicksCounter() {
        Log.d("wow", "saveTotalClicksCounter: getTotalClicksCounter == " + getTotalClicksCounter())
        sharedPrefs?.putInt(SHARED_PREFS_TOTAL_CLICKS_COUNTER, getTotalClicksCounter())
    }
//
//    private fun loadStoreStatus() {
//        StoreModel.getStore(this)
//    }

    private fun initVpIndicator() {
        tabs.setupWithViewPager(viewPager)

        mainActIndicator
            .setSliderColor(ContextCompat.getColor(this, R.color.colorPrimaryDark), ContextCompat.getColor(this, R.color.colorPrimary))
            .setSliderWidth(resources.getDimension(R.dimen.dp_17))
            .setSliderHeight(resources.getDimension(R.dimen.dp_5))
            .setSlideMode(IndicatorSlideMode.WORM)

            .setIndicatorStyle(IndicatorStyle.CIRCLE)
            .setupWithViewPager(viewPager)
    }

    private fun playMerryXmas() {
//        sfxPlayer?.stop()
        sfxPlayer?.start()
    }

    private fun playStoreSfxClip() {
        storeSfxPlayer?.start()
    }

    private fun initSharedPrefs() {
        sharedPrefs = SharedPreferencesManager.getInstance(applicationContext)
    }

    private fun playMusic() {
        musicPlayer = MediaPlayer.create(this, R.raw.bg_music)
        musicPlayer?.setOnPreparedListener {
            it.setScreenOnWhilePlaying(true)
            it.isLooping = true
            it.setVolume(MUSIC_VOLUME, MUSIC_VOLUME)
            it.start()
        }
    }

    override fun onPause() {
        super.onPause()
        musicPlayer?.pause()
//        sfxPlayer?.pause()
        saveAllGameData()
    }

    override fun onStop() {
        super.onStop()
//        musicPlayer?.stop()
//        sfxPlayer?.stop()
        saveAllGameData()
    }

    private fun saveAllGameData() {
        saveTotalClicksCounter()
        saveTreeLevel()
        saveGameLevel()
        saveSnowmanLevel()
        saveIsSnowRuined()
        saveIsSnowCollectable()
        saveIsFirstTimeInGame()
        saveMisstleToe()
        saveCps()
        saveCpc()

        setIsSnowmanCollectable(true)

        //        saveCoins(getTreeFragment().getCoins())
    }

    override fun onResume() {
        super.onResume()
        musicPlayer?.start()

        initPollFish()

        //todo add
//        showLuckySpinnerDialogFragment()
    }

    override fun onDestroy() {
        super.onDestroy()
        musicPlayer?.apply {
            stop()
            release()
        }
        musicPlayer = null
        sfxPlayer?.apply {
            stop()
            release()
        }
        sfxPlayer = null
    }

    override fun onNewIntent(intent: Intent?) {
        super.onNewIntent(intent)
        val url = intent?.data.toString()
        Xmas3Cheater.processCheat(this, url)
    }

    private fun initUi() {
        customFont = ResourcesCompat.getFont(this, R.font.xmas_font)

        viewPager.setCurrentItem(TREE_SCREEN_POS, true)

        // start winter snowflakes
//        winter.startWinter()
    }

    override fun startSnow() {
        winter.startWinter()
        snowfall.visibility = View.VISIBLE
    }

    private fun initViewPager() {
        totalClicksCounter  = sharedPrefs?.getInt(SHARED_PREFS_TOTAL_CLICKS_COUNTER, 0) ?: 0
        currentTreeLvl = getCurrentTreeLevel()
        currentCoins = sharedPrefs?.getInt(SHARED_PREFS_COINS_PARAM, 0) ?: 0
        currentCps = getCPS()
        currentCpc = getCPC()
        Log.d("wow", "initViewPager:: cpc == $currentCpc")
        Log.d("wow", "initViewPager: sharedPrefs == null ? " + (if(sharedPrefs == null) "true" else "false"))
        Log.d("wow", "initViewPager: totalClicksCounter == $totalClicksCounter")
        viewPagerAdapter = ViewPagerAdapter(this, supportFragmentManager, currentCoins, currentCps, currentCpc, totalClicksCounter)
        viewPager = findViewById(R.id.view_pager)
        viewPager.adapter = viewPagerAdapter

        viewPager.addOnPageChangeListener(object : OnPageChangeListener {
            override fun onPageScrolled(
                position: Int,
                positionOffset: Float,
                positionOffsetPixels: Int
            ) {
                val x =
                    ((viewPager.width * position + positionOffsetPixels) * computeFactor()).toInt()
                scrollView.scrollTo(x, 0)
            }

            override fun onPageSelected(position: Int) {}
            override fun onPageScrollStateChanged(state: Int) {}
            private fun computeFactor(): Float {
                return (background.width - viewPager.width) /
                        (viewPager.width * (viewPagerAdapter!!.count - 1)).toFloat()
            }
        })
    }

    private fun initFab() {
        val fab: FloatingActionButton = findViewById(R.id.fab)

        fab.setOnClickListener { view ->
            Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                .setAction("Action", null).show()
        }
    }

    override fun onBackPressed() {
        saveAllGameData()
        Shutdown.now(this)
    }

    override fun popUpText(text: String, newGameLvl: Int) {
        CommonConfetti.rainingConfetti(rootView, intArrayOf(
            Color.CYAN,
            Color.BLUE,
            Color.MAGENTA,
            Color.RED,
            Color.YELLOW,
            Color.GREEN,
            Color.LTGRAY,
            Color.MAGENTA,
            Color.YELLOW,
            Color.GREEN,
            Color.LTGRAY,
            Color.MAGENTA,
            Color.RED,
            Color.YELLOW,
            Color.GREEN,
            Color.LTGRAY,
            Color.DKGRAY
        )).stream(4200)



        mHandler?.removeCallbacks(hideEarnedCoinsText)
        earnedCoinsText.visibility = View.VISIBLE
        earnedCoinsText.setBadgeMainText(text)
        earnedCoinsText.setBadgeSubText(" $newGameLvl ")

        mHandler?.postDelayed(
            hideEarnedCoinsText,
            Random.nextLong(MEDIUM_WAIT_TIME, THREE_SECONDS_WAIT_TIME)
        )
    }

    override fun popUpMessage(text: String) {
        mHandler?.removeCallbacks(hideBoughtItemText)
        boughtItemText.visibility = View.VISIBLE
        boughtItemText.text = text
        mHandler?.postDelayed(hideBoughtItemText, Random.nextLong(THREE_SECONDS_WAIT_TIME, LONG_WAIT_TIME))


        playStoreSfxClip()
    }

    override fun setToolbarVisibility(isVisible: Boolean) {
        if(isVisible) {
            toolbar.visibility = View.VISIBLE
        } else {
            toolbar.visibility = View.GONE
        }
    }

    override fun showMessage(text: String) {
        mHandler?.post {
            Snackbar.make(rootView, text, Snackbar.LENGTH_LONG)
//                .setAction("Action", null)
                .show()
//            Toast.makeText(this, text, Toast.LENGTH_LONG).show()
        }
    }

    override fun setCurrentSnowMan(storeItem: StoreItem) {
        val treeFragment = getTreeFragment()

        currentSnowmanLvl = storeItem.id

        setCurrentSnowmanLevel(currentSnowmanLvl)
//        setIsStoreItemBought(storeItem.name + storeItem.id)


        treeFragment?.setSnowMan(true)

        viewPager.setCurrentItem(TREE_SCREEN_POS, true)
    }

    override fun setCurrentTree(storeItem: StoreItem) {
        val treeFragment = getTreeFragment()
        setTreeLevel(getStoreItemCurrentLvl(storeItem))
        treeFragment?.updateCurrentTreeAnimation()

        viewPager.setCurrentItem(TREE_SCREEN_POS, true)
        //todo think
    }

    override fun buyStoreItem(
        storeItem: StoreItem,
        catName: String
    ) {
        subtractCoins(storeItem.price)
        setIsStoreItemBought(storeItem.name + storeItem.id)
        getTreeFragment()?.updateCoins(getCoins())
        val categoryName = when (catName) {
            UPGRADES_SECTION_NAME -> {
                "Upgrade"
            }
            TREES_SECTION_NAME -> {
                "Tree"
            }
            SNOWMANS_SECTION_NAME -> {
                "Snowman"
            }
            else -> {
                catName
            }
        }
        popUpMessage("NEW $categoryName:\n${storeItem.name}")

        if(catName == UPGRADES_SECTION_NAME) {
            if(storeItem.id == 420) {
                //Auto clicker Elf - CPS
                buyOrUpgradeAutoClicker(storeItem)
            } else if(storeItem.id == 421) {
                //Cookie man - CPC
                buyOrUpgradeCookieMan(storeItem)            //todo test for multiple subtractCoins calls
            } else if(storeItem.id == 555) {
                //Misstletoe - santa visit rate
                buyMisttletoe(storeItem)
            }
        } else if(catName == TREES_SECTION_NAME) {
//            setStoreTreeItemCurrentLvl(storeItem, storeItem.currentUpgradeLvl+1)
            setCurrentTree(storeItem)
            //todo rethink
        } else if (catName == SNOWMANS_SECTION_NAME) {
            setCurrentSnowMan(storeItem)

            //todo Impl
        }

    }

    fun buyMisttletoe(storeItem: StoreItem) {
        val treeFragment = getTreeFragment()

        treeFragment?.updateCoins(getCoins())
        treeFragment?.showMistletoe(true)
        buyMisstleToe()

        setStoreItemCurrentLvl(storeItem, storeItem.currentUpgradeLvl + 1)

        viewPager.setCurrentItem(TREE_SCREEN_POS, true)
    }

    override fun buyOrUpgradeCookieMan(storeItem: StoreItem) {
        val treeFragment = getTreeFragment()

        if(storeItem.currentUpgradeLvl > storeItem.minUpgradeLvl) {
            val price = storeItem.upgradeScalar * 10 * storeItem.currentUpgradeLvl
            subtractCoins(price)
            popUpMessage("Upgraded Cookie:\n${storeItem.name} Level ${storeItem.currentUpgradeLvl}")
        }

        Log.d("wow", "cpc :: $currentCpc")

//        treeFragment?.buyCookieMan()
        treeFragment?.updateCoins(getCoins())
        treeFragment?.updateCps(upgradeCPC())
//        treeFragment?.coinsPerClick

        setStoreItemCurrentLvl(storeItem, storeItem.currentUpgradeLvl + 1)

        viewPager.setCurrentItem(TREE_SCREEN_POS, true)
    }

    override fun upgradeClicker(storeItem: StoreItem) {
        val price = storeItem.upgradeScalar * 10 * storeItem.currentUpgradeLvl
        subtractCoins(price)

        popUpMessage("Upgraded Item:\nAuto Clicker Elf")

        buyOrUpgradeAutoClicker(storeItem)
    }

    private fun buyOrUpgradeAutoClicker(storeItem: StoreItem) {
        val treeFragment = getTreeFragment()

        treeFragment?.upgradeAutoClicker()
        treeFragment?.updateCoins(getCoins())

        setStoreItemCurrentLvl(storeItem, storeItem.currentUpgradeLvl + 1)

        viewPager.setCurrentItem(TREE_SCREEN_POS, true)
    }

    override fun fixSnowman(price: Int) {
        if(currentCoins >= price) {
            subtractCoins(price)

            getTreeFragment()?.updateCoins(getCoins())

            setIsRuinedSnowman(false)
            setIsSnowmanCollectable(false)

        } else {
            showMessage("You need $price coins")
        }
    }

    override fun upgradeTree(storeItem: StoreItem, treeLvl: Int) {
        val price = storeItem.upgradeScalar * 10 * storeItem.currentUpgradeLvl
        if(currentCoins >= price) {
            subtractCoins(price)
            setStoreItemCurrentLvl(storeItem, treeLvl)
            getTreeFragment()?.updateCoins(getCoins())

            popUpMessage("Upgraded Tree:\n${storeItem.name}")

            setCurrentTree(storeItem)
        } else {
            showMessage("You need $price coins")
        }
    }

    override fun onTreeClick(coins: Int) {
        //todo retouch
//        mHandler?.post {

            FloatingToast.makeToast(this, "  Earned Coins: ${coins.getFormattedString()}  ", FloatingToast.LENGTH_SHORT)
                .setFadeOutDuration(FloatingToast.FADE_DURATION_QUICK)
                .setTextSizeInDp(26f)
                .setBackgroundBlur(true)
                .setFloatDistance(42f)
                .setTextColor(whiteColor)
                .setShadowLayer(18f, 0.42f, 0.42f, blackColor)
                .setTextTypeface(customFont)
                .setGravity(GRAVITY_CENTER, Random.nextInt(-21, 21), Random.nextInt(-42, 42)).show()
//        }

    }

    // get fragments

    override fun getBuyFragment(): BuyFragment? {
        return if (viewPagerAdapter == null) null else viewPagerAdapter?.getItem(BUY_SCREEN_POS) as BuyFragment
    }

    override fun getTreeFragment(): TreeFragment? {
        return if (viewPagerAdapter == null) null else viewPagerAdapter?.getItem(TREE_SCREEN_POS) as TreeFragment
    }

    override fun getStoreFragment(): StoreFragment? {
        return if (viewPagerAdapter == null) null else viewPagerAdapter?.getItem(STORE_SCREEN_POS) as StoreFragment
    }


    override fun goToViewPagerFrag(fragPos: Int) {
        viewPager.setCurrentItem(fragPos, true)
    }
    /**

    Shared Prefs Operations


     */

    override fun getIsStoreItemBought(uniqueStoreItemId: String) : Boolean {
        return sharedPrefs?.getBoolean(SHARED_PREFS_IS_BOUGHT_STORE_ITEM + uniqueStoreItemId, false) ?: false
    }

    override fun setIsStoreItemBought(uniqueStoreItemId: String) {
        sharedPrefs?.putBoolean(SHARED_PREFS_IS_BOUGHT_STORE_ITEM + uniqueStoreItemId, true)
    }


    override fun getCoins(): Int {
        if(currentCoins <= -1) {
            currentCoins = sharedPrefs?.getInt(SHARED_PREFS_COINS_PARAM, 0) ?: 0
        }


//        if(BuildConfig.DEBUG) {
//            currentCoins = Int.MAX_VALUE/2
//        }


        return currentCoins
    }

    override fun addCoins(newPlusCoinsAmount: Int) {
        val newCoins = currentCoins + newPlusCoinsAmount
        currentCoins = newCoins
    }

    override fun subtractCoins(newMinusCoinsAmount: Int) {
        val newCoins = currentCoins - newMinusCoinsAmount
        currentCoins = newCoins
    }

    override fun saveCoins(coinsCounter: Int) {
        currentCoins = coinsCounter
        sharedPrefs?.putInt(SHARED_PREFS_COINS_PARAM, coinsCounter)
    }

    override fun getCurrentSnowmanLevel(): Int {
        if(currentSnowmanLvl == -1) {
            currentSnowmanLvl = sharedPrefs?.getInt(SHARED_PREFS_SNOW_MAN_LEVEL_PARAM, 0) ?: 0
        }
        return currentSnowmanLvl
    }

    override fun setCurrentSnowmanLevel(snowManLevel: Int) {
        currentSnowmanLvl = snowManLevel
    }

    override fun getIsRuinedSnowman(): Boolean {
        Log.d("wow", "is from sharedprefs == " + if(isSnowmanRuined == null) "true" else "false")
        if (isSnowmanRuined == null) {
            isSnowmanRuined = sharedPrefs?.getBoolean(SHARED_PREFS_IS_SNOW_MAN_RUINED_PARAM, false) ?: false
        }
        Log.d("wow", "isSnowmanRuined == $isSnowmanRuined")
        return isSnowmanRuined!!
    }

    override fun setIsRuinedSnowman(isRuined: Boolean) {
        isSnowmanRuined = isRuined
    }

    override fun getIsSnowmanCollectable(): Boolean {
        Log.d("wow", "is from sharedprefs == " + if(isSnowmanCollectable == null) "true" else "false")
        if (isSnowmanCollectable == null) {
            isSnowmanCollectable = sharedPrefs?.getBoolean(SHARED_PREFS_IS_SNOW_MAN_COLLECTABLE_PARAM, true) ?: true
        }
        Log.d("wow", "getIsSnowmanCollectable == $isSnowmanCollectable")
        return isSnowmanCollectable!!
    }

    override fun getIsFirstTimeInGame(): Boolean {
        isFirstTimeInGame = sharedPrefs?.getBoolean(IS_FIRST_TIME_IN_GAME_PARAM, true) ?: isFirstTimeInGame
        Log.d("wow", "getIsFirstTimeInGame == $isFirstTimeInGame")
        return isFirstTimeInGame
    }

    override fun setIsFirstTimeInGame() {
        isFirstTimeInGame = false
    }

    override fun setIsSnowmanCollectable(isCollectable: Boolean) {
        isSnowmanCollectable = isCollectable
    }

    override fun getMisstleToe(): Boolean {
        if (isMisstleToe == null) {
            isMisstleToe = sharedPrefs?.getBoolean(SHARED_PREFS_IS_BOUGHT_MISSTLETOE_PARAM, false) ?: false
        }
        return isMisstleToe!!
    }

    override fun buyMisstleToe() {
        isMisstleToe = true
        Log.d("wow", "buyMisstleToe: isMisstleToe == $isMisstleToe")
    }

    override fun getCurrentTreeLevel() : Int {
        if(currentTreeLvl == -1) {
            currentTreeLvl = sharedPrefs?.getInt(SHARED_PREFS_TREE_LEVEL_PARAM, 1) ?: 1
        }
        return currentTreeLvl
    }

    override fun getStoreItemCurrentLvl(storeItem: StoreItem): Int {
        return sharedPrefs?.getInt(SHARED_PREFS_CURRENT_LEVEL_PARAM + storeItem.name + storeItem.id, storeItem.minUpgradeLvl) ?: storeItem.minUpgradeLvl
    }

    override fun setStoreItemCurrentLvl(storeItem: StoreItem, currentTreeLvl: Int) {
        sharedPrefs?.putInt(SHARED_PREFS_CURRENT_LEVEL_PARAM + storeItem.name + storeItem.id, currentTreeLvl)
    }

    override fun getCPC() : Int {
        val treeFragment = getTreeFragment()
        if(currentCpc == -1) {
            currentCpc = sharedPrefs?.getInt(SHARED_PREFS_COINS_PER_CLICK_PARAM, 1) ?: 1
        } else if(treeFragment != null && treeFragment.isAdded) {
            treeFragment.coinsPerClick
        }
        return currentCpc
    }


    override fun setCPS(cps: Float) {
        currentCps = cps
    }

    override fun setCPC(cpc: Int) {
        currentCpc = cpc
    }

    override fun upgradeCPC() : Int {
        //todo rethink
        val newCpc = currentCpc * 2
//        setCPS(newCps)
        Log.d("wow", "newCPC: $newCpc")
        currentCpc = newCpc
        return newCpc
    }

    override fun getCPS() : Float {
        val treeFragment = getTreeFragment()
        if(currentCps == -1f) {
            currentCps = sharedPrefs?.getFloat(SHARED_PREFS_CLICKS_PER_SEC_PARAM, 0f) ?: 0f
        } else if(treeFragment != null && treeFragment.isAdded) {
            treeFragment.clickPerSec
        }
        return currentCps
    }

    override fun upgradeCPS() : Float {
        //todo rethink
        val newCps = /*currentCps * 2 + */(currentCps / 2) + 0.5f
//        setCPS(newCps)
        Log.d("wow", "newCPS: $newCps")
        currentCps = newCps
        return newCps
    }

    override fun setTreeLevel(treeLvl: Int) {
        currentTreeLvl = treeLvl
    }


    private fun saveGameLevel() {
        sharedPrefs?.putInt(SHARED_PREFS_GAME_LEVEL_PARAM, currentGameLvl)
    }

    private fun saveIsSnowRuined() {
        Log.d("wow", "saveIsRuined: isSnowmanRuined == $isSnowmanRuined")

        isSnowmanRuined?.let{
            sharedPrefs?.putBoolean(SHARED_PREFS_IS_SNOW_MAN_RUINED_PARAM, it)
        }
    }

    private fun saveIsFirstTimeInGame() {
        Log.d("wow", "saveIsSnowCollectable: isFirstTimeInGame == $isFirstTimeInGame")

        isFirstTimeInGame?.let{
            sharedPrefs?.putBoolean(IS_FIRST_TIME_IN_GAME_PARAM, it)
        }
    }

    private fun saveIsSnowCollectable() {
        Log.d("wow", "saveIsSnowCollectable: isSnowmanCollectable == $isSnowmanCollectable")

        isSnowmanCollectable?.let{
            sharedPrefs?.putBoolean(SHARED_PREFS_IS_SNOW_MAN_COLLECTABLE_PARAM, it)
        }
    }

    private fun saveMisstleToe() {
            Log.d("wow", "saveMisstleToe: isMisstleToe == $isMisstleToe")
        if(isMisstleToe != null && isMisstleToe!!){
            sharedPrefs?.putBoolean(SHARED_PREFS_IS_BOUGHT_MISSTLETOE_PARAM, true)
        }
    }
    override fun getGameLevel(): Int {
        Log.d("wow", "getGameLevel: currentGameLvl == $currentGameLvl")
        if(currentGameLvl == -1) {
            currentGameLvl = sharedPrefs?.getInt(SHARED_PREFS_GAME_LEVEL_PARAM, 1) ?: 1
        }
//        if(currentGameLvl == )
        Log.d("wow", "getGameLevel: return currentGameLvl == $currentGameLvl")
        return currentGameLvl
    }

    override fun activateStoreTutorial(tutorialState: Int) {
        val storeFragment = getStoreFragment()

        storeFragment?.startStoreTutorial(tutorialState)

//        val tabs: BadgedTabLayout = findViewById(R.id.tabs)


//        tabs.setBadgeFont(ResourcesCompat.getFont(this, R.font.xmas_font_2))
        storeBadgeCounter++
        setBadgeText(2, storeBadgeCounter.toString())
    }

    override fun setBadgeText(index: Int, badgeText: String?) {
        if(badgeText == null){
            when(index) {
                0 -> {
                    moreBadgeCounter = 0
                }
                1 -> {
                    treeBadgeCounter = 0
                }
                2-> {
                    storeBadgeCounter = 0
                }
            }
        }

        tabs.setBadgeText(index,badgeText)
    }

    private fun saveTreeLevel() {
        sharedPrefs?.putInt(SHARED_PREFS_TREE_LEVEL_PARAM, currentTreeLvl)
    }

    private fun saveSnowmanLevel() {
        sharedPrefs?.putInt(SHARED_PREFS_SNOW_MAN_LEVEL_PARAM, currentSnowmanLvl)
    }

    private fun saveCps() {
        Log.d("wow", "saveCps:  currentCps: ${getCPS()}")
        sharedPrefs?.putFloat(SHARED_PREFS_CLICKS_PER_SEC_PARAM, getCPS())
    }

    private fun saveCpc() {
        Log.d("wow", "saveCpc:  currentCpc: ${getCPC()}")
        sharedPrefs?.putInt(SHARED_PREFS_COINS_PER_CLICK_PARAM, getCPC())
    }

    fun showLuckySpinnerDialogFragment() {
//        onPause()
        //todo try stopping changes in background color
        LuckySpinnerDialogFragment().show(supportFragmentManager,LUCKY_SPINNER_DIALOG_FRAG)
    }

    override fun updateGameLevel() {
        playMerryXmas()

//        winter.stopWinter()
        snowfall.stopFalling()

        mHandler?.postDelayed({
//            winter.startWinter()
            snowfall.visibility = View.GONE
            snowfall.visibility = View.VISIBLE

        }, Random.nextLong(7200,8500))

//        currentTreeLvl++
//        setTreeLevel(currentTreeLvl)

        currentGameLvl++

//        Toast.makeText(baseContext, "Level $currentGameLvl", Toast.LENGTH_SHORT).show()
        popUpText("Level", currentGameLvl)

//        saveTotalClicksCounter()

        //get store item and upgrade it's level
//        storeItem.currentUpgradeLvl

    }

    override fun onPollStarted() {
        onPause()
        tabs.visibility = View.GONE
        mainActIndicator.visibility = View.INVISIBLE

        viewPager.setPagingEnabled(false)
    }

    override fun onPollFinished() {
        onResume()
        tabs.visibility = View.VISIBLE
        mainActIndicator.visibility = View.VISIBLE

        viewPager.setPagingEnabled(true)
    }
}