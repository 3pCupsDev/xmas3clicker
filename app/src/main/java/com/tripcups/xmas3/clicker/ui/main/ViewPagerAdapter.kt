package com.tripcups.xmas3.clicker.ui.main

import android.content.Context
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import com.tripcups.xmas3.clicker.common.*
import com.tripcups.xmas3.clicker.ui.buy.BuyFragment
import com.tripcups.xmas3.clicker.ui.store.StoreFragment
import com.tripcups.xmas3.clicker.ui.tree.TreeFragment


class ViewPagerAdapter(
    private val context: Context,
    fm: FragmentManager,
    private val currentCoins: Int,
    private val currentCPS: Float,
    private val currentCPC: Int,
    private val totalClicksCounter: Int
)
    : FragmentStatePagerAdapter(fm, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT) {

    private var storeFrag: StoreFragment
    private var treeFrag: TreeFragment
    private var buyFrag: BuyFragment

    init {
        buyFrag = BuyFragment.newInstance()
        treeFrag = TreeFragment.newInstance(currentCoins, currentCPS, currentCPC,totalClicksCounter)
        storeFrag = StoreFragment.newInstance() 
    }

    override fun getItem(position: Int): Fragment {
        return when(position) {
            BUY_SCREEN_POS -> {
                buyFrag 
            }
            TREE_SCREEN_POS -> {
                treeFrag
            }
            STORE_SCREEN_POS -> {
                storeFrag
            }
            else -> {
                treeFrag
            }
        }
    }

    override fun getPageTitle(position: Int): CharSequence? {
        return context.resources.getString(TAB_TITLES[position])
    }

    override fun getCount(): Int {
        return 3
    }
}

