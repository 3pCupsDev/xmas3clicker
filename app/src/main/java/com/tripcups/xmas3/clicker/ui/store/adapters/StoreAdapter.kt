package com.tripcups.xmas3.clicker.ui.store.adapters

import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import android.widget.ImageView
import android.widget.TextView
import androidx.lifecycle.LifecycleOwner
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.cleveroad.blur_tutorial.BlurTutorial
import com.cleveroad.blur_tutorial.TutorialBuilder
import com.cleveroad.blur_tutorial.listener.SimpleTutorialListener
import com.cleveroad.blur_tutorial.state.tutorial.RecyclerItemState
import com.cleveroad.blur_tutorial.state.tutorial.StateException
import com.cleveroad.blur_tutorial.state.tutorial.TutorialState
import com.hardik.clickshrinkeffect.applyClickShrink
import com.tripcups.xmas3.clicker.R
import com.tripcups.xmas3.clicker.model.Category
import com.tripcups.xmas3.clicker.model.Store
import com.tripcups.xmas3.clicker.model.StoreItem
import com.tripcups.xmas3.clicker.model.StoreModel.Companion.TREES_SECTION_NAME
import kotlinx.android.synthetic.main.category_item.view.*


const val STORE_ITEM = 0
const val LAST_ITEM = 1
class StoreAdapter(private var store: Store, private var listener: StoreAdapterListener,private val lifecycleOwner: LifecycleOwner) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    interface StoreAdapterListener {
        fun getCurrentMoneySituatish(): Int
        fun buyStoreItem(storeItem: StoreItem, catName: String)

        fun setCurrentTree(storeItem: StoreItem)
        fun upgradeTree(storeItem: StoreItem, treeLvl: Int)
        fun showMessage(text: String)
        fun upgradeClicker(storeItem: StoreItem)
        fun upgradeCookie(storeItem: StoreItem)
        fun setCurrentSnowMan(storeItem: StoreItem)
        fun clearStoreTabBadge()

    }

    private var tutorial: BlurTutorial? = null

    private val categoriesVHs = arrayListOf<CategoryViewHolder>()

    companion object {
        private const val VIEW_ID = 0
        private const val MENU_ITEM_ID = 1
        private const val AUTO_CLICKER_ID = 2
        private const val PATH_ID = 3

        private const val FIRST_TREE_UPGRADE = 4
        private const val SECOND_TREE_BUY = 5
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return if(viewType == LAST_ITEM)
            LastItemViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.last_item, parent, false))
        else {
            val catVH = CategoryViewHolder(
                LayoutInflater.from(parent.context).inflate(R.layout.category_item, parent, false)
            )

            categoriesVHs.add(catVH)

            catVH
        }
    }

    override fun getItemCount(): Int {
        return store.categories.size + 1
    }

    override fun getItemViewType(position: Int): Int {
        return if(position < store.categories.size) {
            STORE_ITEM
        } else {
            LAST_ITEM
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if(getItemViewType(position) == STORE_ITEM) {
            val item = getItem(position)
            holder as CategoryViewHolder
            holder.bind(item)
        } else {
            holder as LastItemViewHolder

        }
    }

    private fun getItem(position: Int): Category {
        return store.categories[position]
    }

    fun updateStore(store: Store) {
        this.store = store

//        for(cat in store.categories) {
//
//        }
        notifyDataSetChanged()
    }

    fun showTutorialForStoreItem(storeFragRoot: FrameLayout, state: Int) {
        var storeItem: StoreItem? = null
        val storeItemPrice: Int

        val states: List<RecyclerItemState> = when(state) {
            0 -> {
                storeItem = categoriesVHs[0].category.storeItems[0]
                storeItemPrice = storeItem.price
                listOf(RecyclerItemState(AUTO_CLICKER_ID, categoriesVHs[0].categoryItemsRv, 0))
            }
            1 -> {
                storeItem = categoriesVHs[1].category.storeItems[0]
                storeItemPrice = storeItem.price
                listOf(RecyclerItemState(FIRST_TREE_UPGRADE, categoriesVHs[1].categoryItemsRv, 0))
            }
            2 -> {
                storeItem = categoriesVHs[1].category.storeItems[1]
                storeItemPrice = storeItem.price
                listOf(RecyclerItemState(SECOND_TREE_BUY, categoriesVHs[1].categoryItemsRv, 1))
            }
            else -> {
                storeItemPrice = 420
                listOf()
            }
        }

        if(states.isEmpty()) {
            return
        }
//        val price = when (states[0].id) {
//            //TODO: get price from storeItem.price
//            AUTO_CLICKER_ID -> 15
//            FIRST_TREE_UPGRADE -> 100
//            SECOND_TREE_BUY -> 220
//            else -> 420
//        }

        val canBuy = listener.getCurrentMoneySituatish() >= storeItemPrice


        if(canBuy && storeItem != null && (!storeItem.isMine || storeItem.currentUpgradeLvl != storeItem.maxUpgradeLvl)) {
            initFirstTimeStoreTutorial(storeFragRoot, states)
        }
    }

    private fun initFirstTimeStoreTutorial(
        storeFragRoot: FrameLayout,
        states: List<RecyclerItemState>
    ) {
        if(states.isEmpty())
            return
        tutorial = TutorialBuilder()
            .withParent(storeFragRoot)
            .withListener(tutorialListener)
            .withPopupLayout(R.layout.store_tutorial_balloon)
            .withPopupCornerRadius(14f)
            .withBlurRadius(18f)
            .build().apply {
                addAllStates(states)
            }

        Handler(Looper.getMainLooper()).postDelayed({
            tutorial?.start()
        },220)
    }

    fun onSaveInstanceState(outState: Bundle) {
        tutorial?.onSaveInstanceState(outState)
    }

    fun onRestoreInstanceState(savedInstanceState: Bundle?) {
        tutorial?.onRestoreInstanceState(savedInstanceState)
    }

    fun onDestroy() {
        tutorial?.onDestroy()
    }

    private val tutorialListener = object : SimpleTutorialListener() {

        override fun onPopupViewInflated(state: TutorialState, popupView: View) {
            popupView.run {
                val upgradeButton = findViewById<TextView>(R.id.upgradeButton)
                val buyButton = findViewById<TextView>(R.id.buyButton)
                val tvTitle = findViewById<TextView>(R.id.desc)
                val name = findViewById<TextView>(R.id.name)
                val logo = findViewById<ImageView>(R.id.logo)

                buyButton.applyClickShrink()
                upgradeButton.applyClickShrink()


                val price = when (state.id) {
                    //TODO: get price from storeItem.price
                    AUTO_CLICKER_ID -> 15
                    FIRST_TREE_UPGRADE -> 100
                    SECOND_TREE_BUY -> 220
                    else -> 420
                }

                upgradeButton.visibility = when (state.id) {
                    FIRST_TREE_UPGRADE -> View.VISIBLE
                    else -> View.GONE
                }

                upgradeButton.text = when (state.id) {
                FIRST_TREE_UPGRADE -> "UPGRADE\n\n$price"
                else -> ""
            }

                val canBuy = listener.getCurrentMoneySituatish() >= price

                buyButton.isActivated = when (state.id) {
                    FIRST_TREE_UPGRADE -> true
                    SECOND_TREE_BUY -> !canBuy
                    else -> false
                }

                buyButton.isEnabled =  when (state.id) {
                    AUTO_CLICKER_ID -> true
                    SECOND_TREE_BUY -> true
                    else -> false
                }


                buyButton.text = when (state.id) {
                    AUTO_CLICKER_ID,
                    SECOND_TREE_BUY -> "BUY  \n$price"
                    FIRST_TREE_UPGRADE -> "USE"
                    else -> "BUY \n420"
                }

                val icon = when (state.id) {
                    AUTO_CLICKER_ID -> R.raw.elves
                    FIRST_TREE_UPGRADE -> R.drawable.first_tree_anim
                    SECOND_TREE_BUY -> R.drawable.simple_tree_anim
                    else -> R.drawable.simple_tree_anim
                }
                Glide.with(context).load(icon).into(logo)

                name.text = when (state.id) {
                    AUTO_CLICKER_ID -> "CLICKER"
                    FIRST_TREE_UPGRADE -> "FIRST"
                    SECOND_TREE_BUY -> "SIMPLE"
                    else -> "ITEM"
                }
                tvTitle.text = when (state.id) {
                    AUTO_CLICKER_ID -> "Auto clicker elf  \nwill help you gain  \ncoins faster"
                    FIRST_TREE_UPGRADE -> "This is the first \ntree"
                    SECOND_TREE_BUY -> "Simple Xmas tree"
                    else -> ""
                }

                buyButton.setOnClickListener {
                    when (state.id) {
                        AUTO_CLICKER_ID -> listener.buyStoreItem(categoriesVHs[0].category.storeItems[0],categoriesVHs[0].category.name)
                        FIRST_TREE_UPGRADE -> {}
                        else -> listener.buyStoreItem(categoriesVHs[1].category.storeItems[1],categoriesVHs[1].category.name)
                    }
                    tutorial?.next()
                }

                upgradeButton.setOnClickListener {
                    when (state.id) {
                        AUTO_CLICKER_ID -> {}
                        FIRST_TREE_UPGRADE -> listener.upgradeTree(categoriesVHs[1].category.storeItems[0],categoriesVHs[0].category.storeItems[0].currentUpgradeLvl)
                    }
                    tutorial?.next()
                }

                setOnClickListener {
                    if((state.id == AUTO_CLICKER_ID || state.id == FIRST_TREE_UPGRADE /*state.id == SECOND_TREE_BUY*/) && canBuy) {

                    } else {
                        tutorial?.next()
                    }
                }
            }
        }

        override fun onStateError(state: TutorialState, error: StateException) {
            super.onStateError(state, error)
            listener.clearStoreTabBadge()
        }

        override fun onStateExit(state: TutorialState) {
            super.onStateExit(state)
            listener.clearStoreTabBadge()
        }
    }

    inner class LastItemViewHolder(v: View) : RecyclerView.ViewHolder(v) {

    }

    inner class CategoryViewHolder(v: View) : RecyclerView.ViewHolder(v),
        ItemsAdapter.ItemsAdapterListener {

        lateinit var category: Category
        private lateinit var categoryItemsAdapter: ItemsAdapter

        private val categoryName: TextView = v.categoryName
        private val categoryDescription: TextView = v.categoryDescription
        private val comingSoonLayout: View = v.comingSoon
        val categoryItemsRv: RecyclerView = v.categoryItemsRv

        fun bind(category: Category) {
            this.category = category
            this.categoryName.text = category.name
            this.categoryDescription.text = category.description

            comingSoonLayout.visibility = if(category.isAvailable) {
                View.GONE
            } else {
                View.VISIBLE
            }

//            category.storeItems
            val lm = if(category.name == TREES_SECTION_NAME) {
                GridLayoutManager(itemView.context, 4, RecyclerView.VERTICAL, false)
            } else {
                LinearLayoutManager(itemView.context, RecyclerView.HORIZONTAL, false)
            }

            categoryItemsRv.layoutManager = lm
            categoryItemsAdapter = ItemsAdapter(category, this, lifecycleOwner)
            categoryItemsRv.adapter = categoryItemsAdapter
        }

        override fun canBuy(cost: Int): Boolean {
            return listener.getCurrentMoneySituatish() >= cost
        }

        override fun buyItem(storeItem: StoreItem) {
            listener.buyStoreItem(storeItem, category.name)
        }

        override fun upgradeClicker(storeItem: StoreItem) {
            listener.upgradeClicker(storeItem)
        }

        override fun upgradeCookie(storeItem: StoreItem) {
            listener.upgradeCookie(storeItem)
        }

        override fun setCurrentTree(storeItem: StoreItem) {
            listener.setCurrentTree(storeItem)
        }

        override fun setCurrentSnowMan(storeItem: StoreItem) {
            listener.setCurrentSnowMan(storeItem)
        }

        override fun upgradeTree(storeItem: StoreItem, treeLvl: Int) {
            listener.upgradeTree(storeItem, treeLvl)
        }

        override fun showMessage(text: String) {
            listener.showMessage(text)
        }

//        override fun isStoreItemIsMine(storeItem: StoreItem): Boolean {
//            return listener.isStoreItemIsMine(storeItem)
//        }

//        override fun onStoreItemClick(storeItem: StoreItem) {
////            if(storeItem.isMine){
////                listener.onStoreItemClick(storeItem)
////            } else {
////                listener.showMessage("")
////            }
//        }
    }
}
