package com.tripcups.xmas3.clicker.ads

import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import com.google.android.gms.ads.AdRequest
import com.google.android.gms.ads.AdSize
import com.google.android.gms.ads.AdView
import com.google.android.gms.ads.MobileAds

abstract class AdsActivity(layoutId: Int) : AppCompatActivity(layoutId) {

    abstract val adUnitId: String

    fun initAds() {
        MobileAds.initialize(this)
    }

    fun loadBanner(container: ViewGroup) {
        AdView(this).apply {
            adSize = AdSize.BANNER
            adUnitId = this@AdsActivity.adUnitId
            container.addView(this)
            loadAd(AdRequest.Builder().build())
        }
    }
}