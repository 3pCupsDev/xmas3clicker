package com.tripcups.xmas3.clicker.ui.lucky_spinner

import android.graphics.BitmapFactory
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.fragment.app.DialogFragment
import com.bluehomestudio.luckywheel.WheelItem
import com.tripcups.xmas3.clicker.R
import kotlinx.android.synthetic.main.fragment_lucky_spinner_dialog.*
import java.util.ArrayList


class LuckySpinnerDialogFragment : DialogFragment() {

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initLuckyWheel()
        initUi()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    private fun initLuckyWheel() {
        if(context == null) return
        val wheelItems: MutableList<WheelItem> = ArrayList()

        wheelItems.add(
            WheelItem(
                ContextCompat.getColor(context!!, R.color.light_blue),
                BitmapFactory.decodeResource(resources, R.drawable.ic_action_name),
                "420 Coins"
            )
        )

        wheelItems.add(
            WheelItem(
                ContextCompat.getColor(context!!, R.color.darker_red),
                BitmapFactory.decodeResource(resources, R.drawable.ic_action_name),
                "Cookie Man"
            )
        )

        wheelItems.add(
            WheelItem(
                ContextCompat.getColor(context!!, R.color.dark_green),
                BitmapFactory.decodeResource(resources, R.drawable.ic_action_name),
                "555 Coins"
            )
        )

        wheelItems.add(
            WheelItem(
                ContextCompat.getColor(context!!, R.color.purple),
                BitmapFactory.decodeResource(resources, R.drawable.ic_action_name)
                , "6,000 Coins!!"
            )
        )

        wheelItems.add(
            WheelItem(
                ContextCompat.getColor(context!!, R.color.colorPrimaryDark),
                BitmapFactory.decodeResource(resources, R.drawable.ic_action_name),
                "Mistletoe"
            )
        )

        wheelItems.add(
            WheelItem(
                ContextCompat.getColor(context!!, R.color.dark_green),
                BitmapFactory.decodeResource(resources, R.drawable.ic_action_name),
                "666 Coins"
            )
        )

        luckySpinner.addWheelItems(wheelItems)
        luckySpinner.setTarget(5)
    }

    private fun initUi() {

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_lucky_spinner_dialog, container, false)
    }
}