package com.tripcups.xmas3.clicker.cheats

import com.tripcups.xmas3.clicker.ui.main.MainActivity
import kotlin.math.min

object Xmas3Cheater {

    private const val shouldProcessCheats: Boolean = true

    /**
     *
     * CHEAT CODES
     *
     **/
    private const val CHEAT_CODE_DEEP_LINK_BASE_URL = "https://xmas3clicker.web.app/itay"
    private const val CHEAT_CODE_DEEP_LINK_SET_COINS_CHEAT = "https://xmas3clicker.web.app/itay/addCoins="
    private const val CHEAT_CODE_DEEP_LINK_SHOW_SANTA_CHEAT2 = "https://xmas3clicker.web.app/itay/santa"

    fun processCheat(mainActivity: MainActivity, url: String) {
        if(shouldProcessCheats && url.startsWith(CHEAT_CODE_DEEP_LINK_BASE_URL)) {
            if(url.startsWith(CHEAT_CODE_DEEP_LINK_SET_COINS_CHEAT)) {
                val coinsAmount = try{
//                    url.replace(CHEAT_CODE_DEEP_LINK_SET_COINS_CHEAT, "")
                    url.substring(      // we dont do -1 because of '=' inside of the 'CHEAT_CODE_DEEP_LINK_SET_COINS_CHEAT' const
                        CHEAT_CODE_DEEP_LINK_SET_COINS_CHEAT.length,
                        min(CHEAT_CODE_DEEP_LINK_SET_COINS_CHEAT.length + 8, url.length)
                    ).toInt()
                } catch (e: Throwable) {
                    0
                }
                if(coinsAmount <=0) {
                    mainActivity.showMessage("please enter normal number")
                } else {
                    val treeFragment = mainActivity.getTreeFragment()

                    treeFragment?.addCoins(coinsAmount)
                    mainActivity.popUpText("וואווו!!!!", coinsAmount)
                }
            }
            else if(url.startsWith(CHEAT_CODE_DEEP_LINK_SHOW_SANTA_CHEAT2)) {
                val treeFragment = mainActivity.getTreeFragment()
                treeFragment?.apply {
                    mainActivity.goToViewPagerFrag()     //go to tree frag
                    mainActivity.mHandler?.postDelayed({
                        startCrossingSanta()
                    }, 680)
                }

            }
            else {
                mainActivity.showMessage("Wrong Cheat Code\uD83E\uDD2A")
            }
        }
    }
}