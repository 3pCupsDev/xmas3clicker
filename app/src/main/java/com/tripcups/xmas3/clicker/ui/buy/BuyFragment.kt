package com.tripcups.xmas3.clicker.ui.buy

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import com.hardik.clickshrinkeffect.applyClickShrink
import com.tripcups.xmas3.clicker.R
import com.tripcups.xmas3.clicker.common.InAppReviewManager
import com.tripcups.xmas3.clicker.ui.main.MainActivityListener
import kotlinx.android.synthetic.main.fragment_buy.*

class BuyFragment : Fragment() {

    interface BuyFragmentListener : MainActivityListener{

    }

    companion object {
        fun newInstance() = BuyFragment()
    }

    private lateinit var viewModel: BuyViewModel
    private var listener: BuyFragmentListener? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_buy, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initUi()
    }

    private fun initUi() {
        comingSoonGif.applyClickShrink()
        comingSoonGif.setOnClickListener {
            activity?.let { act ->
                listener?.showMessage("If you enjoy " + getString(R.string.app_name) + " please consider leaving a review")
                InAppReviewManager(act).getInstance().openReviewDialog()
            }
        }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(this).get(BuyViewModel::class.java)
        // TODO: Use the ViewModel
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if(context is BuyFragmentListener) {
            listener = context as BuyFragmentListener
        }
        else throw RuntimeException(context.toString())
    }

    override fun onResume() {
        super.onResume()
//        listener?.setToolbarVisibility(false)
    }
}