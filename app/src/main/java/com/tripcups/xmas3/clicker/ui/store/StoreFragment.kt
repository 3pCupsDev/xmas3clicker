package com.tripcups.xmas3.clicker.ui.store

import android.content.Context
import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.tripcups.xmas3.clicker.R
import com.tripcups.xmas3.clicker.common.setCoins
import com.tripcups.xmas3.clicker.model.Store
import com.tripcups.xmas3.clicker.model.StoreItem
import com.tripcups.xmas3.clicker.model.StoreModel
import com.tripcups.xmas3.clicker.ui.main.MainActivity
import com.tripcups.xmas3.clicker.ui.main.MainActivityListener
import com.tripcups.xmas3.clicker.ui.store.adapters.StoreAdapter
import kotlinx.android.synthetic.main.fragment_store.*

class StoreFragment : Fragment(R.layout.fragment_store), StoreAdapter.StoreAdapterListener {

    private var tutorialState: Int = 0

    //    private var tutorial: BlurTutorial? = null
    private var coinsCounter: Int = 0
    private lateinit var storeAdapter: StoreAdapter
    private var store: Store = Store()

    private var shouldShowTutorialNow = false

    interface StoreFragmentListener : MainActivityListener {
        fun getIsStoreItemBought(uniqueStoreItemId: String) : Boolean
        fun setIsStoreItemBought(uniqueStoreItemId: String)

        fun buyStoreItem(storeItem: StoreItem, catName: String)
        fun buyOrUpgradeCookieMan(storeItem: StoreItem)

        fun upgradeTree(storeItem: StoreItem, treeLvl: Int)
        fun upgradeClicker(storeItem: StoreItem)
    }

    companion object {
        fun newInstance() = StoreFragment()
    }

    private var listener: StoreFragmentListener? = null
    private lateinit var viewModel: StoreViewModel

    override fun onAttach(context: Context) {
        super.onAttach(context)

        if (context is StoreFragmentListener) {
            listener =  context as StoreFragmentListener
        } else {
            throw RuntimeException(context.toString() )
        }
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewModel = ViewModelProviders.of(this).get(StoreViewModel::class.java)

//        initObservers()
        initRecycler()
        initCoins()
        updateLevelText()
        initStoreItems()
        initUi()
    }



//    private fun initFirstTimeStoreTutorial() {
//
//        val states by lazy {
//
//            val path = Path().apply {
//                addCircle(storeFragRv.get(0).pivotX, storeFragRv.get(0).pivotY, 300F, Path.Direction.CCW)
//            }
//
//            listOf(
////                ViewState(VIEW_ID, storeFragRv),
////            MenuState(MENU_ITEM_ID, tbMenuOwner, R.id.menu_item),
//                RecyclerItemState(RECYCLER_ITEM_ID, storeFragRv, 0),
//                RecyclerItemState(RECYCLER_ITEM_ID2, storeFragRv, 1),
//                PathState(PATH_ID, path))
//        }
//
//        tutorial = TutorialBuilder()
//            .withParent(storeFragRoot)
//            .withListener(tutorialListener)
//            .withPopupLayout(R.layout.store_tutorial_balloon)
//            .withPopupCornerRadius(14f)
//            .withBlurRadius(20f)
//            .build().apply { addAllStates(states) }
//    }
//
//    private val tutorialListener = object : SimpleTutorialListener() {
//
//        override fun onPopupViewInflated(state: TutorialState, popupView: View) {
//            popupView.run {
//                val buyButton = findViewById<TextView>(R.id.buyButton)
//                val tvTitle = findViewById<TextView>(R.id.desc)
//                val name = findViewById<TextView>(R.id.name)
//                val upgradeButton = findViewById<TextView>(R.id.upgradeButton)
//
//                Glide.with(context).load(R.raw.elves)
//                name.text = when (state.id) {
////                    VIEW_ID -> "clicker"
////                    MENU_ITEM_ID -> "222"
//                    RECYCLER_ITEM_ID -> "clicker"
//                    RECYCLER_ITEM_ID2 -> "strawvery iils"
//                    PATH_ID -> "4444"
//                    else -> "nope"
//                }
//                tvTitle.text = when (state.id) {
////                    VIEW_ID -> ""
////                    MENU_ITEM_ID -> "222"
//                    RECYCLER_ITEM_ID -> "Auto clicker elf will help you gain coins faster"
//                    RECYCLER_ITEM_ID2 -> "strawvery iils"
//                    PATH_ID -> "4444"
//                    else -> "nope"
//                }
//
//                buyButton.setOnClickListener {
//                    when (state.id) {
////                        VIEW_ID -> "1111111"
////                        MENU_ITEM_ID -> "222"
//                        RECYCLER_ITEM_ID -> "buy Auto clicker"
//                        RECYCLER_ITEM_ID2 -> "buy first tree upgrade"
////                        PATH_ID -> "4444"
//                        else -> "nope"
//                    }
//                    tutorial?.next()
//                }
//
////                setOnClickListener {
////                    tutorial?.next()
////                }
//            }
//        }
//    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        storeAdapter.onSaveInstanceState(outState)
    }

    override fun onViewStateRestored(savedInstanceState: Bundle?) {
        super.onViewStateRestored(savedInstanceState)
        storeAdapter.onRestoreInstanceState(savedInstanceState)
    }

    override fun onDestroy() {
        storeAdapter.onDestroy()
        super.onDestroy()
    }



    private fun initStoreItems() {
        //TODO use VM
        handleStoreDetails(StoreModel.getStore(activity as MainActivity))
    }

    fun updateLevelText() {
        val lvl = listener?.getGameLevel()?: 1
        storeFragLvlText?.text = "Level $lvl "
    }

    private fun initCoins() {
        updateCoinsText()
        context?.let{
            Glide
                .with(it)
                .asGif()
                .load(R.raw.coin)
                .into(storeFragCoinGif)
        }
    }

    fun updateCoinsText() {
        //load from VM stored data from shared prefs
        coinsCounter = listener?.getCoins()?: coinsCounter
        storeFragCoinsText?.setCoins(coinsCounter)

//        initStoreItems()
//        storeAdapter.notifyDataSetChanged()
    }

//    private fun initObservers() {
//        viewModel.storeData.observe(viewLifecycleOwner, Observer {
//            storeDetails -> handleStoreDetails(storeDetails)
//        })
//    }

    private fun handleStoreDetails(storeDetails: Store?) {
        storeDetails?.let {
            store = it
            storeAdapter.updateStore(it)
        }
    }

    private fun initRecycler() {
        storeFragRv.layoutManager = LinearLayoutManager(context, RecyclerView.VERTICAL, false)
        storeAdapter = StoreAdapter(store, this, viewLifecycleOwner)
        storeFragRv.adapter = storeAdapter
    }

    private fun initUi() {

    }

    override fun onResume() {
        super.onResume()
        updateLevelText()
        updateCoinsText()
        initStoreItems()

        if(shouldShowTutorialNow){
            storeAdapter.showTutorialForStoreItem(storeFragRoot, tutorialState)
            shouldShowTutorialNow = false
        }

//        listener?.setToolbarVisibility(false)
    }

    fun startStoreTutorial(tutorialState: Int) {
        shouldShowTutorialNow = true
        this.tutorialState = tutorialState
    }

    override fun getCurrentMoneySituatish(): Int {
        return coinsCounter
    }

    override fun buyStoreItem(storeItem: StoreItem, catName: String) {
        if(coinsCounter >= storeItem.price) {

            listener?.buyStoreItem(storeItem, catName)

            updateCoinsText()
            initStoreItems()
//            viewModel.buyStoreItem(storeItem)
        } else {
            listener?.showMessage("You Need ${storeItem.price} Coins")
        }
    }

    override fun upgradeClicker(storeItem: StoreItem) {
        listener?.upgradeClicker(storeItem)
    }

    override fun upgradeCookie(storeItem: StoreItem) {
        listener?.buyOrUpgradeCookieMan(storeItem)
    }

    override fun setCurrentSnowMan(storeItem: StoreItem) {
        listener?.setCurrentSnowMan(storeItem)
    }

    override fun clearStoreTabBadge() {
        listener?.setBadgeText(2, null)
    }

    override fun upgradeTree(storeItem: StoreItem, treeLvl: Int) {
        listener?.upgradeTree(storeItem, treeLvl)
    }

    override fun setCurrentTree(storeItem: StoreItem) {
        listener?.setCurrentTree(storeItem)
    }

    override fun showMessage(text: String) {
        listener?.showMessage(text)
    }

//    override fun isStoreItemIsMine(storeItem: StoreItem): Boolean {
//        return listener.isStoreItemIsMine(storeItem)
//    }




//    override fun onStoreItemClick(storeItem: StoreItem) {
//        //openStoreItemInfoTooltip
//
//    }
}