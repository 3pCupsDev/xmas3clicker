package com.tripcups.xmas3.clicker.common

import android.widget.TextView
import java.text.DecimalFormat

fun Int.getFormattedString() : String {
    return DecimalFormat("#,###").format(this)
}

fun TextView.setCoins(number: Int) {
    this.text = " ${number.getFormattedString()} "
}