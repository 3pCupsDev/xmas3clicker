package com.tripcups.xmas3.clicker.common

import android.content.Context
import android.content.SharedPreferences

class SharedPreferencesManager private constructor(context: Context){

    private var sharedPrefs: SharedPreferences? = null

    init {
        sharedPrefs = context.applicationContext.getSharedPreferences(SHARED_PREFS_FILE, Context.MODE_PRIVATE)
    }

    companion object {

        @Volatile private var INSTANCE: SharedPreferencesManager? = null

        fun getInstance(context: Context): SharedPreferencesManager {
            INSTANCE ?: synchronized(this) {
                if (INSTANCE == null) {
                    INSTANCE = SharedPreferencesManager(context)
                }
            }

            return INSTANCE as SharedPreferencesManager
        }
    }

    fun getInt(key: String, defaultValue: Int): Int {
        return sharedPrefs?.getInt(key, defaultValue) ?: defaultValue
    }

    fun putInt(key: String, value: Int) {
          with (sharedPrefs?.edit()) {
            this?.putInt(key, value)
            this?.apply()
        }
    }

    fun getFloat(key: String, defaultValue: Float): Float {
        return sharedPrefs?.getFloat(key, defaultValue) ?: defaultValue
    }

    fun putFloat(key: String, value: Float) {
        with (sharedPrefs?.edit()) {
            this?.putFloat(key, value)
            this?.apply()
        }
    }

    fun getBoolean(key: String, defaultValue: Boolean): Boolean {
        return sharedPrefs?.getBoolean(key, defaultValue) ?: defaultValue
    }

    fun putBoolean(key: String, value: Boolean) {
        with (sharedPrefs?.edit()) {
            this?.putBoolean(key, value)
            this?.apply()
        }
    }
}