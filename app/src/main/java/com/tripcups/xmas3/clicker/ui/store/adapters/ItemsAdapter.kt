package com.tripcups.xmas3.clicker.ui.store.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.core.content.ContextCompat
import androidx.lifecycle.LifecycleOwner
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.hardik.clickshrinkeffect.applyClickShrink
import com.skydoves.balloon.*
import com.tripcups.xmas3.clicker.R
import com.tripcups.xmas3.clicker.common.SNOWMAN_SECTION
import com.tripcups.xmas3.clicker.common.TREES_SECTION
import com.tripcups.xmas3.clicker.common.UPGRADES_SECTION
import com.tripcups.xmas3.clicker.common.getFormattedString
import com.tripcups.xmas3.clicker.model.Category
import com.tripcups.xmas3.clicker.model.StoreItem
import com.tripcups.xmas3.clicker.model.StoreModel.Companion.TREES_SECTION_NAME
import kotlinx.android.synthetic.main.store_item.view.*


class ItemsAdapter(private val category: Category, private val listener: ItemsAdapterListener, private val lifecycleOwner: LifecycleOwner) : RecyclerView.Adapter<ItemsAdapter.StoreItemViewHolder>() {
    interface ItemsAdapterListener {
        fun canBuy(cost: Int) : Boolean
        fun buyItem(storeItem: StoreItem)
        fun setCurrentTree(storeItem: StoreItem)
        fun setCurrentSnowMan(storeItem: StoreItem)
//        fun setTreeLevel(treeLvl: Int)
        fun showMessage(text: String)
        fun upgradeTree(storeItem: StoreItem, treeLvl: Int)
        fun upgradeClicker(storeItem: StoreItem)
        fun upgradeCookie(storeItem: StoreItem)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): StoreItemViewHolder {
        return StoreItemViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.store_item, parent, false))
    }

    override fun getItemCount(): Int {
        return category.storeItems.size
    }

    override fun onBindViewHolder(holder: StoreItemViewHolder, position: Int) {
        val item = getItem(position)
        holder as StoreItemViewHolder
        holder.bind(item)
    }

    private fun getItem(position: Int): StoreItem {
        return category.storeItems[position]
    }

    inner class StoreItemViewHolder(v: View) : RecyclerView.ViewHolder(v) {

        private var nextUpgradePrice: Int = 0
        private var isStoreItemUpgradable = false
        var isMyTree = false
        var isMyUpgrade = false
        var isMySnowman = false

        private lateinit var storeItem: StoreItem
        private val image: ImageView = v.itemImage
        private val name: TextView = v.storeItemName
        private val card: CardView = v.storeItemCardView

        init {
            card.applyClickShrink()
            card.setOnClickListener {
//                listener.onStoreItemClick(storeItem)
                showBalloon()
            }
        }

        private fun showBalloon() {
            val balloon = itemView.context?.let {
                val iconForm = IconForm.Builder(it)
                    .setDrawable(ContextCompat.getDrawable(it, R.drawable.snowflake))
                    .setIconColor(ContextCompat.getColor(it, R.color.colorPrimary))
                    .setDrawableGravity(IconGravity.TOP)
                    .setIconSize(10)
                    .setIconSpace(12)
                    .build()


                val balloonHeight = when {
                    isMyTree -> {
                        290
                    }
                    category.name == TREES_SECTION_NAME -> {
                        252
                    }
                    storeItem.id == 555 -> {        //misstletoe
                        300
                    }
                    isMyUpgrade -> {
                        286
                    }
                    else -> {
                        275
                    }
                }

                Balloon.Builder(it)
                    .setArrowSize(16)
                    .setLayout(R.layout.store_item_balloon)
                    .setArrowOrientation(ArrowOrientation.BOTTOM)
                    .setArrowConstraints(ArrowConstraints.ALIGN_ANCHOR)
                    .setArrowPosition(0.5f)
                    .setArrowVisible(true)
                    .setAutoDismissDuration(12_000)
                    .setDismissWhenLifecycleOnPause(true)
                    .setDismissWhenShowAgain(true)
                    .setDismissWhenOverlayClicked(true)
                    .setDismissWhenClicked(true)
                    .setDismissWhenTouchOutside(true)
                    .setCircularDuration(280)
                    .setWidthRatio(0.32f)
                    .setHeight(balloonHeight + 38)
                    .setCornerRadius(14f)
                    .setMarginBottom(32)
                    .setAlpha(0.95f)
//                    .setTextColor(ContextCompat.getColor(it, R.color.colorPrimaryDark))
//                    .setText(storeItem.description)
//                    .setTextSize(14f)
//                    .setTextIsHtml(true)
//                    .setIconForm(iconForm)
//                    .setIconDrawable(ContextCompat.getDrawable(it, R.drawable.snowflake))
                    .setBackgroundColor(ContextCompat.getColor(it, R.color.white))
//                .setOnBalloonClickListener(onBalloonClickListener)
                    .setBalloonAnimation(BalloonAnimation.CIRCULAR)
                .setLifecycleOwner(lifecycleOwner)
                    .build()
            }


            balloon?.getContentView()?.let {
                initBalloonLayout(it, balloon)
            }

            balloon?.dismiss()

            balloon?.show(itemView)
        }

        private fun initBalloonLayout(
            balloon: View,
            balloonDialog: Balloon
        ) {
            val logo: ImageView = balloon.findViewById(R.id.logo)
            val name: TextView= balloon.findViewById(R.id.name)
            val desc: TextView= balloon.findViewById(R.id.desc)
            val buyButton: TextView = balloon.findViewById(R.id.buyButton)
            val upgradeButton: TextView = balloon.findViewById(R.id.upgradeButton)

//            val isStoreItemUpgradable = storeItem.currentUpgradeLvl < storeItem.maxUpgradeLvl
//            val nextUpgradePrice = storeItem.upgradeScalar * 10 * storeItem.currentUpgradeLvl

            buyButton.applyClickShrink()
            upgradeButton.applyClickShrink()

            upgradeButton.setOnClickListener {
                if(storeItem.isMine && isStoreItemUpgradable) {
                        if(listener.canBuy(nextUpgradePrice)){
                            itemView.storeItemName.isEnabled = true
                            itemView.storeItemName.isActivated = false
                            name.isActivated = false

                            if(category.id == TREES_SECTION) {
                                listener.upgradeTree(storeItem, storeItem.currentUpgradeLvl + 1)
                            } else if (category.id == UPGRADES_SECTION) {
                                when(storeItem.id) {
                                    420 -> {
                                        listener.upgradeClicker(storeItem)
                                    }
                                    421 -> {
                                        listener.upgradeCookie(storeItem)
                                    }
                                }
                            }
                        } else {
                            listener.showMessage("Not enough coins to buy " + storeItem.name)
                        }

                    balloonDialog.dismiss()
                }
            }
            buyButton.setOnClickListener {
                balloonDialog.dismiss()

                if(storeItem.isMine) {
                    if(category.id == TREES_SECTION){
                        listener.setCurrentTree(storeItem)
                    } else if(category.id == SNOWMAN_SECTION) {
                        listener.setCurrentSnowMan(storeItem)
                    }
                } else {
                    listener.buyItem(storeItem)
                }
            }

            itemView.context.let {
                val resId = if (storeItem.imgResId != 0) {
                    storeItem.imgResId
                } else {
                    R.drawable.snowflake
                }

                name.text = storeItem.name
                desc.text = storeItem.description
                buyButton.text = "BUY\n${storeItem.price.getFormattedString()}"

//                storeItem.minUpgradeLvl
                if(storeItem.isMine /*|| listener.*/) {
                    //bought it
                    buyButton.isActivated = true
                    buyButton.isEnabled = true

                    if(category.id == TREES_SECTION){
                        buyButton.text = "USE"
                        //todo distinguish between Use and Using
                    } else if(category.id == UPGRADES_SECTION) {
                        if(storeItem.id == 420 || storeItem.id == 421) {
                            buyButton.visibility = View.GONE
                        }
                    } else if(category.id == SNOWMAN_SECTION) {
                        buyButton.text = "USE"

                    } else {
                        buyButton.text = "BOUGHT"
                    }

                    if(isMyTree || isMyUpgrade) {
                        upgradeButton.visibility = View.VISIBLE
                        if(isStoreItemUpgradable) {
                            upgradeButton.text = "UPGRADE\n\n${nextUpgradePrice.getFormattedString()}"
                            if(listener.canBuy(nextUpgradePrice)) {
                                upgradeButton.isActivated = false
                                upgradeButton.isEnabled = true
                            } else {
                                //upgradable but cant buy
                                upgradeButton.isActivated = false
                                upgradeButton.isEnabled = false
                            }
                        } else {
                            if(storeItem.minUpgradeLvl != storeItem.maxUpgradeLvl) {
                                upgradeButton.text = "UPGRADED"
                            } else if(storeItem.id == 555) // misstletoe
                            {
                                upgradeButton.visibility = View.GONE
                            }

                            else {
                                upgradeButton.text = "NO\nUPGRADES"
                            }
                            upgradeButton.isActivated = true
                            upgradeButton.isEnabled = true
                        }
                    } else if(isMySnowman) {
                        upgradeButton.visibility = View.GONE
                    } else {
                        upgradeButton.visibility = View.GONE
                    }

                } else if (listener.canBuy(storeItem.price)){
                    // not mine but can buy
                    upgradeButton.visibility = View.GONE

                    buyButton.isActivated = false
                    buyButton.isEnabled = true
                } else {
                    //not mine and cant buy
                    upgradeButton.visibility = View.GONE

                    buyButton.isActivated = false
                    buyButton.isEnabled = false

//                    buyButton.text
                }

                Glide.with(it)
                    .load(resId)
                    .placeholder(R.drawable.snowflake)
                    .override(150)
//                    .circleCrop()
                    .into(logo)
            }
        }

        fun bind(storeItem: StoreItem) {
            this.storeItem = storeItem

            isMyTree = category.id == TREES_SECTION && storeItem.isMine
            isMyUpgrade = category.id == UPGRADES_SECTION && storeItem.isMine
            isMySnowman = category.id == SNOWMAN_SECTION && storeItem.isMine
            isStoreItemUpgradable = storeItem.currentUpgradeLvl < storeItem.maxUpgradeLvl
            nextUpgradePrice = storeItem.upgradeScalar * 10 * storeItem.currentUpgradeLvl

            name.text = storeItem.name

            name.isActivated = if(storeItem.isMine && isStoreItemUpgradable && listener.canBuy(nextUpgradePrice)) {
                    //bought and now upgradable
                    name.isEnabled = true
                    false
                } else if(storeItem.isMine) {
                  //bought
//                name.isActivated = true
                name.isEnabled = true
                true
            } else if (listener.canBuy(storeItem.price)) {
                //not mine but can buy
//                name.isActivated = false
                name.isEnabled = true
                false
            } else {
                //not mine && cant buy
//                name.isActivated = false
                name.isEnabled = false
                false
            }

            Glide.with(itemView.context)
                .load(storeItem.imgResId)
                .into(image)

        }
    }
}