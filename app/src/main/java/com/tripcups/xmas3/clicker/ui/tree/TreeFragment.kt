package com.tripcups.xmas3.clicker.ui.tree

import android.animation.ObjectAnimator
import android.animation.ValueAnimator
import android.animation.ValueAnimator.INFINITE
import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Path
import android.media.MediaPlayer
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.util.Log
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import com.bumptech.glide.Glide
import com.cleveroad.blur_tutorial.BlurTutorial
import com.cleveroad.blur_tutorial.TutorialBuilder
import com.cleveroad.blur_tutorial.listener.SimpleTutorialListener
import com.cleveroad.blur_tutorial.state.tutorial.PathState
import com.cleveroad.blur_tutorial.state.tutorial.TutorialState
import com.hardik.clickshrinkeffect.applyClickShrink
import com.skydoves.balloon.ArrowConstraints
import com.skydoves.balloon.ArrowOrientation
import com.skydoves.balloon.Balloon
import com.skydoves.balloon.BalloonAnimation
import com.tripcups.xmas3.clicker.R
import com.tripcups.xmas3.clicker.common.*
import com.tripcups.xmas3.clicker.custom_views.BalloonCreator
import com.tripcups.xmas3.clicker.model.StoreModel
import com.tripcups.xmas3.clicker.ui.main.MainActivityListener
import kotlinx.android.synthetic.main.fragment_tree.*
import java.util.*
import kotlin.math.max
import kotlin.math.min
import kotlin.math.roundToInt
import kotlin.random.Random

class TreeFragment : Fragment(R.layout.fragment_tree) {

    private var tutorialStarted: Boolean = false
    private var startSantaXPos: Float = -1F
    private var startSantaYPos: Float = -1F
    private var clickedSanta: Boolean = false
    private lateinit var autoClickerTimer: Timer
    private lateinit var crossingSantaClicker: Timer
    private var currentTreeLevel = 1
    private var santaCounter = 0
    private var coinsCounter = 0
    var clickPerSec = 0f
    var coinsPerClick = 1
    private var fakeClickPerSec = 0f

    private val mHandler: Handler = Handler(Looper.getMainLooper())
    private lateinit var pageViewModel: TreeViewModel
    private var listener: TreeFragmentListener? = null
    var totalClicksCounter = 0 // restarted every session

    private var treeClickSound: MediaPlayer? = null
    private var scrollingSantaSound: MediaPlayer? = null
    private var santaHoingSound: MediaPlayer? = null

    private var tutorial: BlurTutorial? = null
    private val FIRST_TIME_PATH_ID = 444
    private val FIRST_TIME_PATH_ID2 = 445
    private val FIRST_TIME_PATH_ID3 = 446

    interface TreeFragmentListener : MainActivityListener {
        fun onTreeClick(coins: Int)

        fun setIsSnowmanCollectable(isCollectable: Boolean)
        fun getIsSnowmanCollectable() : Boolean

        fun fixSnowman(fixPrice: Int)
    }

//    private val showToolbarRunnable = Runnable {
//        if(isVisible && isResumed){
//            listener?.setToolbarVisibility(true)
//        }
//    }

    private val hideMoneyRunnable = Runnable {
        if(isVisible) {
            coinsLayout?.visibility = View.GONE
        }
    }

    private val showMoneyRunnable = Runnable {
        if(isVisible) {
            coinsLayout?.visibility = View.VISIBLE
        }
    }


    private val resetFakeCounter = Runnable {
        setCpsText(clickPerSec)
        fakeClickPerSec = clickPerSec
    }

    private val showCPSRunnable = Runnable {
        if(isVisible){
            leftPanels?.visibility = View.VISIBLE
            if (clickPerSec > 0) {
                clickersLayout?.visibility = View.VISIBLE
            }
            if (coinsPerClick > 1) {
                cookieManAnimator?.visibility = View.VISIBLE
                cookieManLayout?.visibility = View.VISIBLE
            }
        }
    }

    private val hideCPSRunnable = Runnable {
        if(isVisible){
            leftPanels?.visibility = View.GONE
            clickersLayout?.visibility = View.INVISIBLE
            cookieManAnimator?.visibility = View.GONE
            cookieManLayout?.visibility = View.GONE
        }
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)

        if (context is TreeFragmentListener) {
            listener = context
        } else {
            throw RuntimeException(context.toString())
        }
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initCoins()
        initClicksLayout()
        initTree()
        initSFX()
        updateCurrentTreeAnimation()
        initCrossingSantaTimer()
        initSnowman()
        initMissleToe()




        showFirstTimeStoreTutorialIfNeeded() // todo finish
    }

    private fun showFirstTimeStoreTutorialIfNeeded() {
        val isFirstTime = listener?.getIsFirstTimeInGame() == true //&& totalClicksCounter <= 0
        if(!isFirstTime) {
            listener?.startSnow()
            return
        }

        val states by lazy {

            val centerX = (this.resources.displayMetrics.widthPixels / 2).toFloat()
            val centerY = (this.resources.displayMetrics.heightPixels / 2).toFloat()


            val path = Path().apply {
                addCircle(
                    centerX,
                    centerY,
                    420F,
                    Path.Direction.CCW
                )
            }

            arrayListOf(
                PathState(FIRST_TIME_PATH_ID, path),
                PathState(FIRST_TIME_PATH_ID2, path),
                PathState(FIRST_TIME_PATH_ID3, path)
            )
        }

        tutorial = TutorialBuilder()
            .withParent(treeFragRoot)
            .withListener(tutorialListener)
            .withPopupLayout(R.layout.tree_tutorial_balloon)
            .withPopupCornerRadius(14f)
            .withBlurRadius(5f)
            .build().apply {
                addAllStates(states)
            }
    }

    private val tutorialListener = object : SimpleTutorialListener() {

        override fun onPopupViewInflated(state: TutorialState, popupView: View) {
            popupView.run {
                val actionButton = findViewById<TextView>(R.id.actionButton)
                val descriptionTv = findViewById<TextView>(R.id.desc)

                actionButton.applyClickShrink()

                actionButton.text = when (state.id) {
                    FIRST_TIME_PATH_ID -> " Hello "
                    FIRST_TIME_PATH_ID2 -> " OK "
                    FIRST_TIME_PATH_ID3 -> " Cool "
                    else -> " OK "
                }

                descriptionTv.text = when (state.id) {
                    FIRST_TIME_PATH_ID -> "Welcome to Xmas 3 Clicker"
                    FIRST_TIME_PATH_ID2 -> "This is your tree"
                    FIRST_TIME_PATH_ID3 -> "Every click on your tree \nwill earn you coins "
                    else -> ""
                }

                val clickListener = View.OnClickListener {
                    when (state.id) {
                        FIRST_TIME_PATH_ID -> {

                            tutorial?.next()
                        }
                        FIRST_TIME_PATH_ID2 -> {

                            tutorial?.next()
                        }
                        FIRST_TIME_PATH_ID3 ->  {
                            listener?.startSnow()
                            listener?.setIsFirstTimeInGame()

                            tutorial?.next()
                        }
                        else -> {
                            tutorial?.next()
                        }
                    }
                }
                actionButton.setOnClickListener(clickListener)
                setOnClickListener(clickListener)
            }
        }
    }

    private val decreaseSantaTapper: Runnable =  Runnable{
        if(santaTapper.currentProgress in 3..18) {
            santaTapper.currentProgress = santaTapper.currentProgress-1
        } else if(santaTapper.currentProgress >= 19) {

            mHandler.postDelayed({
                santaTapper.currentProgress = 20
            }, 120)
//            santaHoingSound?.reset()
            santaHoingSound?.start()
            listener?.popUpMessage("Very Good!")        //todo random gj sentence
            santaTapper.isActivated = false
//        } else {
//            mHandler.removeCallbacks(this@Runnable)
        }
    }

    private fun tapSanta() {
        mHandler.removeCallbacks(decreaseSantaTapper)
        onTreeClick()

        if(santaTapper.currentProgress <=19) {
            santaTapper.currentProgress++
        }
        santaCounter++

        mHandler.postDelayed(decreaseSantaTapper, 129)
    }

    private fun showSantaTapper() {
        santaTapper.visibility = View.VISIBLE
        santaTapper.isActivated = true

        santaTapper.currentProgress = max(1, (clickPerSec.toInt() % 20 / 3))
//        santaTapper.currentProgress = max(1, min(20, ((clickPerSec.toInt() + santaCounter) % 20 / 3)))
        tapSanta()

//        crossingSantaClicker = Timer()
//        crossingSantaClicker.scheduleAtFixedRate(object : TimerTask() {
//            override fun run() {
//                if (isResumed) {
//                    if (isVisible) {
//                        scrollingSantaSound?.start()
//                    } else {
//                        listener?.showMessage("Santa is coming...")
//                    }
//
//                    mHandler.postDelayed({
//                        if (isResumed) {
//                            if (isVisible) {
//                                startCrossingSanta()
//                            } else {
//                                listener?.showMessage("You missed Santa")
//                            }
//                        }
////                }, Random.nextLong(1625,2420))      //debug
//                    }, Random.nextLong(625, 2420))
//                }
//            }
//        })
    }

    private fun initMissleToe() {
        val isMisstleToe = listener?.getMisstleToe() ?: false
        showMistletoe(isMisstleToe)
        Log.d("wow", "initMissleToe: listener == null ?? ${listener == null}")
        Log.d("wow", "initMissleToe: isMisstleToe ==$isMisstleToe")
    }

    private fun initSnowman() {
        val currentSnowmanLevel = listener?.getCurrentSnowmanLevel() ?: 0

        val isSnowmanAvailable = if(currentSnowmanLevel == 0) {
            null
        } else (Random.nextInt(coinsCounter) % 5 == 2 || Random.nextBoolean()) && currentSnowmanLevel != 0       // one in 5 coins every session randomly

        setSnowMan(isSnowmanAvailable)
//        snowManPiled
        snowManPilePb.visibility = View.GONE
        snowManPilePbText.visibility = View.GONE


        snowManPilePb.setColorRes(R.color.colorPrimary)
//        snowManPilePb.setColorRes(R.color.dark_green)
        snowManPilePb.setLoadingColorRes(R.color.dark_green)


        snowManPiled.setOnClickListener {
            showSnowmanBalloon(listener?.getIsRuinedSnowman() ?: false)
        }

        snowManAnimator.setOnClickListener {
            showSnowmanBalloon(false)
        }
    }

    @SuppressLint("SetTextI18n")
    private fun initClicksLayout() {
        autoClickerTimer = Timer()

        //load from VM stored data from shared prefs
        clickPerSec = arguments?.getFloat(CPS_ARG, 0f) ?: 0f
        coinsPerClick = arguments?.getInt(CPC_ARG, 1) ?: 1

        setCpsText(clickPerSec)
        setCpcText(coinsPerClick)
        if(clickPerSec > 0 || coinsPerClick > 1) {
            mHandler.postDelayed(showCPSRunnable, SMALL_WAIT_TIME)
        }

        context?.let{
            Glide
                .with(it)
                .asGif()
                .load(R.raw.elves)
                .into(elfGif)
        }


//        val currentCps = listener?.getCPS() ?: 0f
//        listener?.showMessage(clickPerSec.toString())

        if(clickPerSec > 0) {
            setAutoClicker((clickPerSec * 10).toLong())
        }
    }

    private fun initCoins() {
        //load from VM stored data from shared prefs
        coinsCounter = arguments?.getInt(COIN_COUNT_ARG, 0) ?: 0
        totalClicksCounter = arguments?.getInt(TOTAL_CLICKS_COUNTER_ARG, 0) ?: 0
        Log.d("wow", "initCoins: totalClicksCounter == $totalClicksCounter")

        coinsText.setCoins(coinsCounter)

        if(coinsCounter > 0) {
            mHandler.postDelayed(showMoneyRunnable, SMALL_WAIT_TIME)
        }

        context?.let{
            Glide
                .with(it)
                .asGif()
                .load(R.raw.coin)
                .into(coinGif)
        }
    }

    private fun initTree() {
        treeFragTreeAnimator.applyClickShrink()
        treeFragTreeAnimator.setOnClickListener {
            if(listener?.getIsFirstTimeInGame() == true && !tutorialStarted) {
                mHandler.postDelayed({
                    listener?.goToViewPagerFrag()   //goTo Tree Fragment
                    mHandler.postDelayed({
                        tutorial?.start()
                        tutorialStarted = true
                    }, 860)
                }, 860)
            }

            onTreeClick()
            addStubClicks()

//            listener?.onTreeClick(coinsPerClick)
            totalClicksCounter++


            if(santaTapper.isActivated) {
                tapSanta()
            }

        }
        crossingSantaAnimator.setOnClickListener {
            clickedSanta = true

            if(santaTapper.isActivated) {
                tapSanta()
            } else {
                listener?.popUpMessage("You caught Santa!")
                showSantaTapper()
                crossingSantaAnimator.isClickable = true
            }
        }
    }

    private fun initSFX() {
        treeClickSound = MediaPlayer.create(context, R.raw.tree_click)
        treeClickSound?.setOnPreparedListener {
            it.setVolume(SFX_VOLUME, SFX_VOLUME)
        }
        scrollingSantaSound = MediaPlayer.create(context, R.raw.jingle_bells)
        scrollingSantaSound?.setOnPreparedListener {
            it.setVolume(SPECIAL_SFX_VOLUME, SPECIAL_SFX_VOLUME)
        }
        santaHoingSound = MediaPlayer.create(context, R.raw.merry_xmas_2)
        santaHoingSound?.setOnPreparedListener {
            it.setVolume(MAX_VOLUME, MAX_VOLUME)
        }
    }

    fun startCrossingSanta() {
        if(crossingSantaAnimator.visibility == View.VISIBLE)
            return

            crossingSantaAnimator?.apply {
                santaCounter = 0
                clickedSanta = false

                mHandler.postDelayed({
                    crossingSantaAnimator.isClickable = false
                },5950)

                // PLAY SOUND 3 SEC b4
                scrollingSantaSound?.apply {
                    if(!isPlaying) {
                        start()
                    }
                }

                visibility = View.VISIBLE
                playAnimation()

                ObjectAnimator.ofFloat(this, "translationX", 6420f).apply {
                    duration = 6200
                    start()
                    mHandler.postDelayed({
//                    visibility = View.INVISIBLE
                        end()

                        translationX = startSantaXPos
                        translationY = startSantaYPos


                        if (clickedSanta) {
                            //todo collect money earned

                            santaTapper.isActivated = false
                            mHandler.postDelayed({
                                //todo COLLECT FROM SAnta tapper
                                santaTapper.visibility = View.GONE
                                val totalSantaClickerWinPrice = santaCounter * santaTapper.currentProgress
                                addCoins(totalSantaClickerWinPrice)
                                listener?.popUpText("santa bonus", totalSantaClickerWinPrice)

                            }, 640)

                        } else {
                            // todo show tutorial
                            listener?.showMessage("Try to catch Santa next time..")

                        }

                        clickedSanta = false

                        visibility = View.GONE
                    }, 6500)
                }
            }
    }


    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        tutorial?.onSaveInstanceState(outState)
    }

    override fun onViewStateRestored(savedInstanceState: Bundle?) {
        super.onViewStateRestored(savedInstanceState)
        tutorial?.onRestoreInstanceState(savedInstanceState)
    }


    override fun onResume() {
        super.onResume()
//        mHandler.postDelayed(showToolbarRunnable, MEDIUM_WAIT_TIME)
        mHandler.postDelayed(showCPSRunnable, MEDIUM_WAIT_TIME)
//        mHandler.postDelayed(showMoneyRunnable, MEDIUM_WAIT_TIME)

//        if(BuildConfig.DEBUG){
//            startCrossingSanta()
//        }
    }

    override fun onPause() {
        super.onPause()
        saveCoins()
//        mHandler.removeCallbacks(showCPSRunnable)
//        mHandler.removeCallbacks(showCPSRunnable)

//        treeClickSound?.pause()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        treeClickSound?.apply {
            stop()
            release()
        }
        treeClickSound = null
        scrollingSantaSound?.apply {
            stop()
            release()
        }
        scrollingSantaSound = null
        santaHoingSound?.apply {
            stop()
            release()
        }
        santaHoingSound = null

        saveCoins()

        clearClickerTimer()
        crossingSantaClicker.cancel()
        crossingSantaClicker.purge()


    }

    private fun clearClickerTimer() {
        autoClickerTimer.cancel()
        autoClickerTimer.purge()
    }

    private fun onTreeClick() {
        mHandler.removeCallbacks(hideMoneyRunnable)
        mHandler.removeCallbacks(hideCPSRunnable)
        treeClickSound?.start()

        if(coinsLayout.visibility != View.VISIBLE) {
            coinsLayout.visibility = View.VISIBLE
        }


        coinsCounter+= coinsPerClick
        coinsText.setCoins(coinsCounter)

        listener?.getStoreFragment()?.updateCoinsText()

        handleCounter()
        mHandler.postDelayed(hideMoneyRunnable, LONG_WAIT_TIME)
        mHandler.postDelayed(hideCPSRunnable, LONG_WAIT_TIME)
    }

    fun addCoins(coins: Int) {
        if(coins > 1) {
            coinsCounter += (coins - 1)//todo fix stupid bug here
            listener?.onTreeClick(coins)
        }

//        treeFragTreeAnimator.performClick()
        onTreeClick()
    }

    private fun addStubClicks() {
        if(clickPerSec > 1) {
//            mHandler.removeCallbacks(resetFakeCounter)
            if(fakeClickPerSec < clickPerSec){
                fakeClickPerSec = clickPerSec
            }
            fakeClickPerSec+= coinsPerClick
            setCpsText(fakeClickPerSec)
            mHandler.postDelayed(resetFakeCounter, ONE_SECOND)
        }
    }

    fun updateCoins(coinsCount : Int) {
        coinsCounter = coinsCount
        coinsText?.setCoins(coinsCounter)
        mHandler.post(showMoneyRunnable)
    }

    fun getCoins() : Int {
        return coinsCounter
    }

    private fun initCrossingSantaTimer() {
        crossingSantaClicker = Timer()

        startSantaXPos = crossingSantaAnimator.translationX
        startSantaYPos = crossingSantaAnimator.translationY

        val isMisstleToe = listener?.getMisstleToe() ?: false

        val delayTime = if(isMisstleToe) {
            Random.nextLong(15,42)
        } else {
            Random.nextLong(45,75)
        }

        val periodTime = if(isMisstleToe) {
            Random.nextLong(42,142)
        } else {
            Random.nextLong(69,420)
        }

        crossingSantaClicker.scheduleAtFixedRate(object : TimerTask() {
            override fun run() {
                val isAppResumed = isResumed || listener?.getStoreFragment()?.isResumed == true || listener?.getBuyFragment()?.isResumed  == true
                val minTime: Long
                val maxTime: Long
                if(isAppResumed){
                    scrollingSantaSound?.start()
                    if(isVisible && isResumed){
                        minTime = 990
                        maxTime = 2920
                    } else {
                        listener?.showMessage("Santa is coming...")
                        minTime = 1425
                        maxTime = 3690
                    }

                    mHandler.postDelayed({
                        if(isAppResumed) {
                            if (isVisible && isResumed) {
                                startCrossingSanta()
                            } else {
                                listener?.showMessage("You missed Santa")
                            }
                        }
                    }, Random.nextLong(minTime, maxTime))
                }
            }

//            }, Random.nextLong(12,15) * 1_000, Random.nextLong(10,12) * 1_000)      //debug
        }, delayTime * 1_000, periodTime * 1_000)
    }

    private fun setAutoClicker(everySeconds: Long) {
        if(clickersLayout.visibility != View.VISIBLE) {
            mHandler.post(showCPSRunnable)
        }
        clearClickerTimer() // todo check if needed ???

        autoClickerTimer = Timer()

        autoClickerTimer.scheduleAtFixedRate(object : TimerTask() {
            override fun run() {
                mHandler.post {
                    if(isVisible && (isResumed || activity?.hasWindowFocus() == true)){
                        val cps = listener?.getCPS()?.roundToInt() ?: 0
                        Log.d("wow", "autoClickerTimer.scheduleAtFixedRate:  currentCps: $cps")

                        addCoins(cps)
                    }
                }
            }
        }, 0, min(THREE_SECONDS_WAIT_TIME, max(MEDIUM_WAIT_TIME, max(0, everySeconds * 1000)))) // todo fix

        val newCps = everySeconds.toFloat() / 10
        clickPerSec = newCps
        setCpsText(newCps)
        listener?.setCPS(newCps)

        Log.d("wow", "setAutoClicker:  currentCps: $clickPerSec")
    }

    override fun onDestroy() {
        crossingSantaClicker.cancel()
        crossingSantaClicker.purge()
        super.onDestroy()
    }

    @SuppressLint("SetTextI18n")
    private fun setCpsText(newCps: Float) {
        //todo refactor all of CPS values to be Int instead of Float
        clicksPerSecText?.text = "${ newCps.toInt().getFormattedString()} coins/sec."
    }

    @SuppressLint("SetTextI18n")
    private fun setCpcText(newCpc: Int) {
        coinsPerClickText?.text = "${ newCpc.getFormattedString()} coins/click"
    }

    fun upgradeAutoClicker() {
        val newCps = listener?.upgradeCPS()?.times(100) ?: 0f

        setAutoClicker((newCps).toLong())
    }

    private fun handleCounter() {
        val currentGameLvl = listener?.getGameLevel()
        when(totalClicksCounter) {
            15 -> {
                activity?.let{
                    val storeItem = StoreModel.getItemFromCategoryByIndex(0, 0)

                    if(storeItem != null && !storeItem.isMine) {
                        listener?.popUpMessage("New Item Unlocked:\nAuto Clicker Elf")
                        listener?.activateStoreTutorial(0)
                    }
                }
            }
//            in 42..55 -> {
//                //becomes Level 2
//                if(currentGameLvl != null && currentGameLvl <= 2) {
//                    onGameLvlUp()
//                }
////                upgradeAutoClicker()
//            }
            in 99..135 -> {
                //becomes Level 2
                if(currentGameLvl != null && currentGameLvl <= 1) {

                    activity?.let{
                        val storeItem = StoreModel.getItemFromCategoryByIndex(1, 0)

                        if(storeItem != null && storeItem.currentUpgradeLvl != storeItem.maxUpgradeLvl && canBuy(storeItem.price)) {
                            listener?.popUpMessage("New Tree Upgrade:\nFirst Tree")
                            listener?.activateStoreTutorial(1)
                        }
                    }
                    onGameLvlUp()
                }
            }
            in 170..200 -> {
                //becomes Level 3
                if(currentGameLvl != null && currentGameLvl <= 2) {
                    onGameLvlUp()
                }
            }
            in 260..299 -> {
                //becomes Level 4
                if(currentGameLvl != null && currentGameLvl <= 3) {
                    activity?.let{
                        val storeItem = StoreModel.getItemFromCategoryByIndex(1, 1)

                        if(storeItem != null && !storeItem.isMine && canBuy(storeItem.price)) {
                            listener?.popUpMessage("New Item Unlocked:\nSimple Tree")
                            listener?.activateStoreTutorial(2)
                        }
                    }
                    onGameLvlUp()
                }
            }
            300 -> {
                //becomes Level 5
                if(currentGameLvl != null && currentGameLvl <= 4) {
                    onGameLvlUp()
                }
            }
            in 301..419 -> {
                //becomes level 6 MAX
                 if((totalClicksCounter % 150 == 0) && currentGameLvl != null && currentGameLvl <= 5) {
                    onGameLvlUp()
                }
            }

            420 -> {
                //becomes level 7 MAX
                onGameLvlUp()
                //special celebration // todo
                startCrossingSanta()
            }

            in 421..665, in 667..9999 -> {
                //becomes level 10 MAX +(1)
                if((totalClicksCounter % 420 == 0) && currentGameLvl != null && currentGameLvl <= 9) {
                    onGameLvlUp()
                }
            }

            666 -> {
                //becomes level 15 MAX
                onGameLvlUp()
                //special celebration // todo
                startCrossingSanta()
            }
        }
    }

    private fun canBuy(price: Int): Boolean {
        return coinsCounter >= price
    }

    private fun onGameLvlUp() {
        listener?.updateGameLevel()
//        listener?.getStoreFragment()?.updateCoinsText()
//        listener?.getStoreFragment()?.updateLevelText()

//        updateCurrentTreeAnimation()
    }

    fun setSnowMan(isAvailable: Boolean? = null) {
        val currentSnowmanLevel = listener?.getCurrentSnowmanLevel() ?: 0
        val snowmanAnimationId = getSnowmanAnimationId(currentSnowmanLevel)

        val isSnowmanRuined = listener?.getIsRuinedSnowman() ?: false

        Log.d("wow", "setSnowMan: currentSnowmanLevel == $currentSnowmanLevel")
        Log.d("wow", "setSnowMan: isAvailable == $isAvailable")
        Log.d("wow", "setSnowMan: isSnowmanRuined == $isSnowmanRuined")
        Log.d("wow", "setSnowMan: snowmanAnimationId == $snowmanAnimationId")

        if(isAvailable != null && isAvailable && !isSnowmanRuined) {
            //show snowman from sharedPrefs
            snowManPiled.visibility = View.INVISIBLE
            snowManPilePb.visibility = View.GONE
            snowManPilePbText.visibility = View.GONE
            snowManAnimator.visibility = View.VISIBLE

            if(snowmanAnimationId != 0 && !isSnowmanRuined) {
                listener?.setIsRuinedSnowman(false)
                snowManAnimator.setAnimation(snowmanAnimationId)
                snowManAnimator.playAnimation()

                showSnowmanBalloon(false)

            } else {
                snowManPiled.visibility = View.VISIBLE
                snowManPilePb.visibility = View.GONE
                snowManPilePbText.visibility = View.GONE
                snowManAnimator.visibility = View.GONE
                listener?.setIsRuinedSnowman(true)

//                showSnowmanBalloon(true)
            }

        } else if (isAvailable != null) {
            //show ruined snowman
            snowManPiled.visibility = View.VISIBLE
            snowManPilePb.visibility = View.GONE
            snowManPilePbText.visibility = View.GONE
            snowManAnimator.visibility = View.GONE

            listener?.setIsRuinedSnowman(true)

            showSnowmanBalloon(true)

        } else {
            //don't show snow man nor snow
            snowManPiled.visibility = View.GONE
            snowManPilePb.visibility = View.GONE
            snowManAnimator.visibility = View.GONE
            snowManPilePbText.visibility = View.GONE
        }
    }

    private fun getSnowmanAnimationId(currentSnowmanLevel: Int): Int {
        return when(currentSnowmanLevel) {
            1 -> {
                snowManAnimator.scale = 0.2f
                snowManAnimator.repeatCount = INFINITE
                R.raw.snowman_first_anim
            }
            2 -> {
                snowManAnimator.scale = 0.4f
                snowManAnimator.repeatCount = 0
                R.raw.snow_deer_anim
            }
            3 -> {
                snowManAnimator.scale = 3.2f
                snowManAnimator.repeatCount = INFINITE
                R.raw.spinning_snowman_anim
            }
             4 -> {
                 snowManAnimator.scale = 1.8f
                 snowManAnimator.repeatCount = INFINITE
                 R.raw.jumpin_snowman
             }
            else -> {
                0
            }
        }
    }

    private fun getSnowmanGifId(currentSnowmanLevel: Int): Int {
        return when(currentSnowmanLevel) {
            1 -> {
                R.drawable.wink_snowman
            }
            2 -> {
                R.drawable.reindeer_snowman
            }
            3 -> {
                R.drawable.spinning_snowman
            }
            4 -> {
                R.drawable.jumping_snowman_anim
            }
            else -> {
                0
            }
        }
    }


    private fun showSnowmanBalloon(snowmanRuined: Boolean) {
        val balloon = context?.let {
            val balloonHeight = if(snowmanRuined) {
                300
            } else {
                325
            }

            val balloonWidth = if(snowmanRuined) {
                0.42f
            } else {
                0.32f
            }

            val balloonAlpha = if (snowmanRuined) {
                0.76f
            } else {
                0.95f
            }

            val marginBottom = if (snowmanRuined) {
                0
            } else {
                110
            }

            Balloon.Builder(it)
                .setArrowSize(22)
                .setLayout(R.layout.snowman_balloon)
                .setArrowOrientation(ArrowOrientation.BOTTOM)
                .setArrowConstraints(ArrowConstraints.ALIGN_ANCHOR)
                .setArrowPosition(0.5f)
                .setArrowVisible(true)
                .setAutoDismissDuration(7_000)
                .setDismissWhenLifecycleOnPause(true)
                .setDismissWhenShowAgain(true)
                .setDismissWhenOverlayClicked(true)
                .setDismissWhenClicked(true)
                .setDismissWhenTouchOutside(true)
                .setMarginBottom(marginBottom)
                .setCircularDuration(280)
                .setWidthRatio(balloonWidth)
                .setHeight(balloonHeight + marginBottom)
                .setCornerRadius(14f)
                .setAlpha(balloonAlpha)
                .setBackgroundColor(ContextCompat.getColor(it, R.color.white))
                .setBalloonAnimation(BalloonAnimation.CIRCULAR)
                .setLifecycleOwner(activity)
                .build()
        }

        //todo use This:
//        BalloonCreator.create(activity,)

        balloon?.getContentView()?.let {
            val gameLvl = listener?.getGameLevel() ?: 1
            val fixPrice: Int = 1250 * gameLvl
            val collectPrice = 250 * gameLvl + Random.nextInt(-69, 142)

            val snowmanGif: ImageView = it.findViewById(R.id.snowmanGif)
//            val name: TextView = it.findViewById(R.id.name)
            val desc: TextView = it.findViewById(R.id.desc)
            val actionButton: TextView = it.findViewById(R.id.actionButton)


            if(snowmanRuined) {
                actionButton.text = "FIX"
                desc.text = "snoman is melted\npay ${fixPrice.getFormattedString()} to fix it"

            } else {
                actionButton.text = "COLLECT"

                R.raw.merry_xmas
                val isCollectable = listener?.getIsSnowmanCollectable() ?: true

                desc.text = if(isCollectable) {
                    actionButton.isActivated = false
                    actionButton.isEnabled = true
                    //todo complete
                    String.format(getString(R.string.click_collect_to_earn_coins), collectPrice.getFormattedString())
                } else {
                    actionButton.isActivated = false
                    actionButton.isEnabled = false

                    "Snowman is currently generating money for you"
                }
            }

            val currentSnowmanLevel = listener?.getCurrentSnowmanLevel() ?: 0
            val snowmanGifResId = getSnowmanGifId(currentSnowmanLevel)
            if (snowmanGifResId != 0 || snowmanGifResId != -1) {
                Glide.with(it).asGif().load(snowmanGifResId).into(snowmanGif)
            }

            actionButton.setOnClickListener {
                if(snowmanRuined) {     // FIX

//                    snowManPilePb
                    //delay few seconds todo
                    if(coinsCounter < fixPrice) {
                        listener?.showMessage("You Need $fixPrice coins")
                        return@setOnClickListener
                    }

                    fixSnowman(fixPrice)

                } else {                // COLLECT
                    //todo collect coins
//                    listener?.collectCoinsFromSnowman()

                    if(coinsCounter < collectPrice){
                        listener?.showMessage("You Need $collectPrice coins")
                        return@setOnClickListener
                    }

                    addCoins(collectPrice)
                    listener?.popUpMessage("Snowman collected")
                    listener?.setIsSnowmanCollectable(false)
                }
                balloon.dismiss()
            }
        }

        balloon?.dismiss()

        balloon?.show(snowManPiled)
    }

    private fun fixSnowman(fixPrice: Int) {
        snowManPilePb.visibility = View.VISIBLE
        snowManPilePbText.visibility = View.VISIBLE
        val currentSnowmanLevel = listener?.getCurrentSnowmanLevel() ?: 1
        snowManPiled.isEnabled = false

        val randParam = Random.nextLong(150, 420) * currentSnowmanLevel
        snowManPilePb.setProgress(1f)

        mHandler.postDelayed({
            snowManPilePb.setProgress(5f)
            mHandler.postDelayed({
                snowManPilePb.setProgress(15f)
                mHandler.postDelayed({
                    snowManPilePb.setProgress(30f)
                    mHandler.postDelayed({
                        snowManPilePb.setProgress(42f)
                        mHandler.postDelayed({
                            snowManPilePb.setProgress(69f)
                            mHandler.postDelayed({
                                snowManPilePb.setProgress(82f)
                                mHandler.postDelayed({
                                    snowManPilePb.setProgress(92f)
                                    mHandler.postDelayed({
                                        snowManPilePb.setProgress(100f)
                                        mHandler.postDelayed({
                                            snowManPiled.isEnabled = true
                                            listener?.fixSnowman(fixPrice)
                                            setSnowMan(true)
                                            listener?.setIsSnowmanCollectable(true)

                                            listener?.popUpMessage("Snowman's Fixed!")
                                        }, 420 + randParam)
                                    }, 570 + randParam)
                                }, 1500 + randParam)
                            }, 420 + randParam)
                        }, 690 + randParam)
                    }, 690 + randParam)
                }, 420 + randParam)
            }, 250 + randParam)
        }, 100 + randParam)
    }

    fun showMistletoe(isShow: Boolean) {
        if(isShow) {
            missletoe.visibility = View.VISIBLE
        } else {
            missletoe.visibility = View.GONE
        }
    }

    fun updateCurrentTreeAnimation() {
        currentTreeLevel = listener?.getCurrentTreeLevel() ?: 1
        //todo switch to different value
        val animationId = getTreeAnimationId(currentTreeLevel)
        treeFragTreeAnimator.setAnimation(animationId)
        treeFragTreeAnimator.playAnimation()

        val isLooping = if (animationId == R.raw.cartoon_tree) {
            ValueAnimator.INFINITE
        } else {
            0
        }

        treeFragTreeAnimator.repeatCount = isLooping
    }

    private fun getTreeAnimationId(treeLvl: Int): Int {
        Log.d("wow", "getTreeAnimationId: treeLvl == $treeLvl")
        return when(treeLvl) {
            1 -> {
                R.raw.small_tree_empty
            }
            2 -> {
                R.raw.small_tree_lvl_1
            }
            3 -> {
                R.raw.basic_tree
            }
            4 -> {
                R.raw.basic_tree_lvl_1
            }
            5 -> {
                R.raw.basic_tree_lvl_2
            }
            6 -> {
                R.raw.basic_tree_lvl_3
            }
            7 -> {
                R.raw.basic_tree_lvl_4
            }
            8 -> {
                R.raw.dark_tree
            }
            9-> {
                R.raw.dark_tree_lvl1
            }
            10 -> {
                R.raw.dark_tree_lvl2
            }
            11 -> {
                R.raw.shield_tree
            }
            12 -> {
                R.raw.cartoon_tree
            }

            14 -> {
                R.raw.lines_level_1
            }
            15 -> {
                R.raw.lines_level_2
            }
            16 -> {
                R.raw.lines_level_3
            }
            17 -> {
                R.raw.lines_tree_full
            }

            20 -> {
                R.raw.simple_tree
            }
            21 -> {
                R.raw.simple_tree_lvl_1
            }
            22 -> {
                R.raw.simple_tree_lvl_2
            }
            23 -> {
                R.raw.simple_tree_lvl_3
            }
//            77 -> {
//                R.raw.real_tree
//            }
            else ->{
                R.raw.small_tree_empty
            }
        }
    }

    private fun saveCoins() {
        listener?.saveCoins(coinsCounter)
    }

    fun updateCps(newCpc: Int) {
        coinsPerClick = newCpc
        setCpcText(coinsPerClick)
    }

    companion object {
        private const val COIN_COUNT_ARG = "coin_counter"
        private const val CPS_ARG = "clicks_per_sec"
        private const val CPC_ARG = "coins_per_click"
        private const val TOTAL_CLICKS_COUNTER_ARG = "totalClicksCounter"

        @JvmStatic
        fun newInstance(
            coinCount: Int,
            clicksPerSec: Float,
            coinsPerClick: Int,
            totalClicksCounter: Int
        ): TreeFragment {
            return TreeFragment().apply {
                arguments = Bundle().apply {
                    putInt(COIN_COUNT_ARG, coinCount)
                    putFloat(CPS_ARG, clicksPerSec)
                    putInt(CPC_ARG, coinsPerClick)
                    putInt(TOTAL_CLICKS_COUNTER_ARG, totalClicksCounter)
                }
            }
        }
    }
}