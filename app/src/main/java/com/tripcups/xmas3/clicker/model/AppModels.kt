package com.tripcups.xmas3.clicker.model

import android.graphics.Color

class StoreItem {
    var id: Int = 0
    var imgResId: Int = 0
    var name: String = ""
    var description: String = ""
    var price: Int = 0
    var color: Int = Color.YELLOW
    var isMine: Boolean = false
//    var isNew: Boolean = false

    var upgradeScalar: Int = 1



    var minUpgradeLvl = 0
    var maxUpgradeLvl = 0
    var currentUpgradeLvl = 0


    var nextUpgradablePrice = upgradeScalar * 10 * currentUpgradeLvl

    constructor()
    constructor(
        id: Int = -1,
        name: String,
        description: String,
        price: Int,
        imgResId: Int,
        isMine: Boolean = false,
        upgradeScalar: Int = 1,
        minUpgradeLvl: Int = 0,
        maxUpgradeLvl: Int = minUpgradeLvl,
        currentUpgradeLvl: Int = minUpgradeLvl
        ) {
        this.id = id
        this.name = name
        this.description = description
        this.price = price
        this.imgResId = imgResId
        this.isMine = isMine

        this.upgradeScalar = upgradeScalar

        this.minUpgradeLvl = minUpgradeLvl
        this.maxUpgradeLvl = maxUpgradeLvl
        this.currentUpgradeLvl = currentUpgradeLvl

        nextUpgradablePrice = upgradeScalar * 10 * currentUpgradeLvl
    }

}
class Category {
    var id: Int = -1
    var name: String = ""
    var storeItems: ArrayList<StoreItem> = arrayListOf()
    var description: String = ""
    var isAvailable: Boolean = false

    constructor()
    constructor(id: Int = -1, catName: String, catDescription: String, items: ArrayList<StoreItem>, available: Boolean = false) {
        this.id = id
        name = catName
        description = catDescription
        storeItems = items
        isAvailable = available
    }
}

class Store {
    var categories: ArrayList<Category> = arrayListOf()

    constructor()
    constructor(categories: ArrayList<Category>) {
        this.categories = categories
    }
}